package jm.util;

import java.util.List;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 14:20
 */

public interface Tree<T> {
    public Tree<T> getParent();
    public List<Tree<T>> getChildren();
    public void setParent(Tree<T> parent);
    public void addChild(Tree<T> child);
    public boolean removeChild(Tree<T> child);
    public T getValue();
}
