package jm.util;

public class Point implements Comparable<Point> {
        public int x;
        public int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point add(Point p) {
            return new Point(x + p.x, y + p.y);
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }

        @Override
        public int compareTo(Point other) {
            if (other == null) {
                return 1;
            }
            if (y == other.y) {
                if (x == other.x) {
                    return 0;
                } else {
                    return x < other.x ? -1 : 1;
                }
            }


            return y < other.y ? -1 : 1;
        }

        public boolean equals(Object other) {
            if (other instanceof Point) {
                return compareTo((Point)other) == 0;
            }
            return false;
        }
    }