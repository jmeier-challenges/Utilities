package jm.util;

import java.util.*;

/**
 * User: T05365A
 * Date: 16.08.12
 * Time: 07:39
 */
public class BestN<E> {

    class Entry implements Comparable {
        private Comparable key;
        private E value;

        @Override
        public int compareTo(Object o) {
            if (o instanceof BestN.Entry) {
                return key.compareTo(((BestN.Entry) o).key);

            } else {
                return 0;
            }
        }

        public Entry(Comparable key, E value) {
            this.key = key;
            this.value = value;
        }
    }

    private int max = 10;
    private TreeSet<Entry> priorityQueue = new TreeSet<Entry>();


    public void put(Comparable key, E value) {
        priorityQueue.add(new Entry(key, value));

        if (priorityQueue.size() > max) {
            priorityQueue.pollFirst();
        }
    }

    public List<E> getBest() {
        List<E> best = new ArrayList<E>();
        for (Entry value : priorityQueue) {
            best.add(value.value);
        }
        return best;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Entry value : priorityQueue) {
            builder.append(value.key).append(": ").append(value.value).append("\n");
        }
        return builder.toString();
    }

    public void setMax(int m) {
        max = m;
    }

	public void add(BestN<E> bestN) {
		setMax(max + bestN.max);
		for (Entry e : bestN.priorityQueue) {
			priorityQueue.add(e);
		}
	}
}
