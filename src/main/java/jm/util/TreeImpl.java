package jm.util;

import java.util.LinkedList;
import java.util.List;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 14:24
 */

public class TreeImpl<T> implements Tree<T>{
    private T value;
    private Tree<T> parent = null;
    private List<Tree<T>> children = new LinkedList<Tree<T>>();

    public Tree<T> getParent() {
        return parent;
    }

    public List<Tree<T>> getChildren() {
        return children;
    }

    public void setParent(Tree<T> parent) {
        this.parent = parent;
    }

    public void addChild(Tree<T> child) {
        children.add(child);
    }

    public boolean removeChild(Tree<T> child) {
        return children.remove(child);
    }

    public T getValue() {
        return value;
    }
}
