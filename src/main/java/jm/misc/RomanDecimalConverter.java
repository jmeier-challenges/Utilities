package jm.misc;

public class RomanDecimalConverter {

  // ---------------------------------------------------------------------

public static void main(String args[]) throws Exception {
	String ct = "LXXXIV CI CVIII CVIII XXXII CIX CI XXXII CXVI CIV CI XXXII CX XCVII CIX CI XXXII CXI CII XXXII CXVI CIV CI XXXII CIX XCVII CX XXXII CXIX CIV CXI XXXII CXIX CXIV CXI CXVI CI XXXII XXXIV LXXXII CXI CIX XCVII CX CV XXXII CV CXVI CI XXXII C CXI CIX CXVII CIX XXXIV XXXII XLIX XLVIII XLVIII XXXII CXVI CV CIX CI CXV XLVI";
	for (String number : ct.split(" ")) {
		System.out.print((char)roman2decimal(number));
	}
}

  // ---------------------------------------------------------------------
  // isRoman (String str)
  //
  // stellt fest, ob es sich um eine gueltige roemische Zahl handelt
  // ---------------------------------------------------------------------

private static boolean isRoman (String str) {
  int i;

  for (i=0; i<str.length(); i++) {
    if ("MDCLXVI".indexOf(str.charAt(i)) == -1) {
      break;
    }
  }

  return i == str.length();
}

  // ---------------------------------------------------------------------
  // isDecimal (String str)
  //
  // stellt fest, ob es sich um eine gueltige DezimalZahl > 0 handelt
  // ---------------------------------------------------------------------

private static boolean isDecimal (String str) {
  int i;

  try {
    i = Integer.parseInt(str);
  } catch (NumberFormatException e) {
    return false;
  }

  return i > 0;
}

  // ---------------------------------------------------------------------
  // roman2decimal (String src)
  //
  // wandelt eine roemische Zahl in eine DezimalZahl um
  // ---------------------------------------------------------------------

private static int roman2decimal (String src) {
  int i, result, digits[];

  digits = new int[src.length()];

  for (i=0; i<digits.length; i++) {
    switch (src.charAt(i)) {
    case 'I': digits[i] = 1; break;
    case 'V': digits[i] = 5; break;
    case 'X': digits[i] = 10; break;
    case 'L': digits[i] = 50; break;
    case 'C': digits[i] = 100; break;
    case 'D': digits[i] = 500; break;
    case 'M': digits[i] = 1000; break;
    }
  }

  result = 0;
  for (i=0; i<digits.length-1; i++) {
    if(digits[i]<digits[i+1]) {
      result-=digits[i];
    } else {
      result+=digits[i];
    }
  }
  result += digits[i];

  return result;
}

  // ---------------------------------------------------------------------
  // decimal2roman (int src)
  //
  // wandelt eine DezimalZahl in eine roemische Zahl um
  // ---------------------------------------------------------------------

private static String decimal2roman (int src) {
  char digits[] = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
  String thousands = "", result = "";
  int rang, digit, i;

  for (i=src/1000; i>0; i--) {
    thousands += "M";
  }
  src %= 1000;

  rang = 0;
  while (src > 0) {
    digit = src % 10;
    src /= 10;
    switch (digit) {
    case 1:
      result = "" + digits[rang] + result;
      break;
    case 2:
      result = "" + digits[rang] + digits[rang] + result;
      break;
    case 3:
      result = "" + digits[rang] + digits[rang] + digits[rang] + result;
      break;
    case 4:
      result = "" + digits[rang] + digits[rang+1] + result;
      break;
    case 5:
      result = "" + digits[rang+1] + result;
      break;
    case 6:
      result = "" + digits[rang+1] + digits[rang] + result;
      break;
    case 7:
      result = "" + digits[rang+1] + digits[rang] + digits[rang] + result;
      break;
    case 8:
      result = "" + digits[rang+1] + digits[rang] + digits[rang] + digits[rang] + result;
      break;
    case 9:
      result = "" + digits[rang] + digits[rang+2] + result;
      break;
    }
    rang += 2;
  }
  return thousands + result;
}

  // ---------------------------------------------------------------------

}