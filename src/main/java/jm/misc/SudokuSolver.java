package jm.misc;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * User: T05365A
 * Date: 11.01.13
 * Time: 08:10
 */
public class SudokuSolver {
	private int max = 9;
	private int maxPos = max * max;
	private int[][] areas = new int[max][];
	private int[][] solutionBoard = null;

	private Set<Integer>[] possibleValues;

	public static void main(String[] args) {
		SudokuSolver solver = new SudokuSolver();

		long start = System.currentTimeMillis();
		/*
		solver.solve(0, getSudoku());
		System.out.println(System.currentTimeMillis() - start);
		solver.printBoard(solver.getSolutionBoard());
		System.out.println();

		start = System.currentTimeMillis();
		solver.solveFast(0, getSudoku());
		System.out.println(System.currentTimeMillis() - start);
		solver.printBoard(solver.getSolutionBoard());
		*/

		start = System.currentTimeMillis();
		solver.solveSmart(getSudoku());
		System.out.println(System.currentTimeMillis() - start);
		solver.printBoard(solver.getSolutionBoard());

	}

	public SudokuSolver() {
		setMax(9);
		areas[0] = new int[]{0, 1, 2, 9, 10, 11, 18, 19, 20};
		areas[1] = new int[]{3, 4, 5, 12, 13, 14, 21, 22, 23};
		areas[2] = new int[]{6, 7, 8, 15, 16, 17, 24, 25, 26};

		areas[3] = new int[]{27, 28, 29, 36, 37, 38, 45, 46, 47};
		areas[4] = new int[]{30, 31, 32, 39, 40, 41, 48, 49, 50};
		areas[5] = new int[]{33, 34, 35, 42, 43, 44, 51, 52, 53};

		areas[6] = new int[]{54, 55, 56, 63, 64, 65, 72, 73, 74};
		areas[7] = new int[]{57, 58, 59, 66, 67, 68, 75, 76, 77};
		areas[8] = new int[]{60, 61, 62, 69, 70, 71, 78, 79, 80};
	}

	public static int[][] getSudoku() {
		return new int[][]
			{
				{2,0,0, 0,7,0, 0,8,0},
				{0,9,5, 0,0,0, 4,0,3},
				{7,0,0, 0,5,0, 0,2,0},

				{0,0,0, 2,1,0, 0,0,6},
				{3,0,6, 0,9,0, 5,0,8},
				{1,0,0, 0,6,8, 0,0,0},

				{0,3,0, 0,4,0, 0,0,1},
				{8,0,1, 0,0,0, 7,9,0},
				{0,6,0, 0,8,0, 0,0,2}
			};
	}

	public boolean solve(int pos, int[][] theBoard) {
		if (pos == maxPos) {
			if (checkBoard(theBoard)) {
				solutionBoard = theBoard;
				return true;
			}
			return false;
		}
		if (isSet(pos, theBoard)) {
			return solve(pos + 1, copyBoard(theBoard));
		}

		for (int i = 0; i < max; i++) {
			theBoard[getY(pos)][getX(pos)] = i + 1;
			if (checkBoard(theBoard)) {
				if (solve(pos + 1, copyBoard(theBoard))) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean solveFast(int pos, int[][] theBoard) {
		if (pos == maxPos) {
			//if (checkBoard(theBoard)) {
				solutionBoard = theBoard;
				return true;
			//}
			//return false;
		}

		if (isSet(pos, theBoard)) {
			return solveFast(pos + 1, theBoard);
		}

		for (int i = 0; i < max; i++) {
			int oldValue = getValue(pos, theBoard);
			int oldPos = pos;
			setValue(pos, i + 1, theBoard);
			if (checkBoard(theBoard)) {
				if (solveFast(pos + 1, theBoard)) {
					return true;
				}
			}
			setValue(oldPos, oldValue, theBoard);
		}
		return false;
	}

	public void solveSmart(int[][] theBoard) {
		System.out.println("first step");
		removeFixValues(theBoard);
		printPossibleValues();
		System.out.println("second step");
		checkForFixValues(theBoard);
		printPossibleValues();
		solveSmartRek(0, theBoard);
	}

	private void checkForFixValues(int[][] theBoard) {
		checkForFixValuesRow();
		checkForFixValuesCol();
		checkForFixValuesArea();
	}

	private void checkForFixValuesRow() {
		for (int row = 0; row < max; row++) {
			for (int value = 1; value <= max; value++) {
				int startPos = row * max;
				int posFound = -1;
				for (int pos = startPos; pos < startPos + max; pos++) {
					if (possibleValues[pos].contains(value)) {
						if (posFound >= 0) {
							posFound = -1;
							break;
						} else {
							posFound = pos;
						}
					}
				}

				if (posFound >= 0) {
					setPossibleValue(posFound, value);
				}
			}
		}
	}

	private void checkForFixValuesCol() {
		for (int col = 0; col < max; col++) {
			for (int value = 1; value <= max; value++) {
				int posFound = -1;
				for (int row = 0; row < max; row++) {
					int pos = row * max + col;
					if (possibleValues[pos].contains(value)) {
						if (posFound >= 0) {
							posFound = -1;
							break;
						} else {
							posFound = pos;
						}
					}
				}

				if (posFound >= 0) {
					setPossibleValue(posFound, value);
				}
			}
		}
	}

	private void checkForFixValuesArea() {
		for (int[] area: areas) {
			for (int value = 1; value <= max; value++) {
				int posFound = -1;
				for (int pos: area) {
					if (possibleValues[pos].contains(value)) {
						if (posFound >= 0) {
							posFound = -1;
							break;
						} else {
							posFound = pos;
						}
					}
				}

				if (posFound >= 0) {
					setPossibleValue(posFound, value);
				}
			}
		}
	}

	private void setPossibleValue(int pos, int value) {
		removePossibleValue(pos, value);
		possibleValues[pos].clear();
		possibleValues[pos].add(value);
	}

	private boolean solveSmartRek(int pos, int[][] theBoard) {
		if (pos == maxPos) {
			solutionBoard = theBoard;
			return true;
		}
		Set<Integer> possibles = possibleValues[pos];
		if (possibles.size() == 1) {
			setValue(pos, possibles.iterator().next(), theBoard);
			return solveSmartRek(pos + 1, theBoard);
		}

		for (int i : possibles) {
			int oldValue = getValue(pos, theBoard);
			int oldPos = pos;
			setValue(pos, i, theBoard);
			if (checkBoard(theBoard)) {
				if (solveSmartRek(pos + 1, theBoard)) {
					return true;
				}
			}
			setValue(oldPos, oldValue, theBoard);
		}
		return false;
	}

	private void removeFixValues(int[][] theBoard) {
		for (int i = 0; i < maxPos; i++) {
			int value = theBoard[getY(i)][getX(i)];
			if (value > 0) {
				setPossibleValue(i, value);
			}
		}
	}

	private void removePossibleValue(int pos, int value) {
		removeFromRow(pos, value);
		removeFromCol(pos, value);
		removeFromArea(pos, value);
	}

	private void removeFromRow(int pos, int value) {
		int rowStartPos = (pos/max)*max;
		for (int rowPos = rowStartPos; rowPos < rowStartPos + max; rowPos++) {
			possibleValues[rowPos].remove(value);
		}
	}

	private void removeFromCol(int pos, int value) {
		int col = pos % max;
		for (int row = 0; row < max; row++) {
			possibleValues[row * max + col].remove(value);
		}
	}

	private void removeFromArea(int aPos, int value) {
		int[] area = getArea(aPos);
		for (int pos : area) {
			possibleValues[pos].remove(value);
		}
	}

	private int[] getArea(int aPos) {
		for (int[] area : areas) {
			for (int pos : area) {
				if (pos == aPos) {
					return area;
				}
			}
		}
		return null;
	}

	private void setValue(int pos, int value, int[][] theBoard) {
		theBoard[getY(pos)][getX(pos)] = value;
	}

	private int getValue(int pos, int[][] theBoard) {
		return theBoard[getY(pos)][getX(pos)];
	}

	private boolean isSet(int pos, int[][] board) {
		return board[pos / max][pos % max] > 0;
	}

	private void printBoard(int[][] theBoard) {
		if (theBoard == null) {
			System.out.println("Board is emtpy");
			return;
		}

		for (int[] row : theBoard) {
			for (int value : row) {
				System.out.print(value + "\t");
			}
			System.out.println();
		}
		System.out.println();
	}

	private boolean checkBoard(int[][] theBoard) {
		if (!checkRows(theBoard)) {
			return false;
		}
		if (!checkCols(theBoard)) {
			return false;
		}
		return checkAreas(theBoard);
	}

	private boolean checkRows(int[][] theBoard) {
		for (int[] row : theBoard) {
			boolean[] cells = new boolean[max];
			for (int c = 0; c < max; c++) {
				int value = row[c];
				if (value > 0) {
					if (cells[value-1]) {
						return false;
					}
					cells[value-1] = value > 0;
				}
			}
		}
		return true;
	}

	private boolean checkCols(int[][] theBoard) {
		for (int col = 0; col < max; col++) {
			boolean[] cells = new boolean[max];
			for (int row = 0; row < max; row++) {
				int value = theBoard[row][col];
				if (value > 0) {
					if (cells[value-1]) {
						return false;
					}
					cells[value-1] = value > 0;
				}

			}
		}
		return true;
	}

	private boolean checkAreas(int[][] theBoard) {
		for (int[] area : areas) {
			boolean[] cells = new boolean[max];
			for (int pos : area) {
				int value = theBoard[pos / max][pos % max];
				if (value > 0) {
					if (cells[value - 1]) {
						return false;
					}
					cells[value - 1] = value > 0;
				}
			}
		}
		return true;
	}

	private int[][] copyBoard(int[][] theBoard) {
		int [][] newBoard = new int[max][];
		for (int i = 0; i < max; i++) {
			newBoard[i] = Arrays.copyOf(theBoard[i], max);
		}
		return newBoard;
	}

	private int getX(int pos) {
		return pos % max;
	}

	private int getY(int pos) {
		return pos / max;
	}

	public void setMax(int newMax) {
		max = newMax;
		maxPos = newMax * newMax;
		initPossibleValues();
	}

	private void initPossibleValues() {
		possibleValues = new Set[maxPos];
		for (int s = 0; s < possibleValues.length; s++) {
			possibleValues[s] = new HashSet<Integer>(max);
			for (int i = 1; i <= max; i++) {
				possibleValues[s].add(i);
			}
		}
	}

	private void printPossibleValues() {
		for (Set<Integer> possibles : possibleValues) {
			for (Integer value : possibles) {
				System.out.print(value + ", ");
			}
			System.out.println();
		}

	}

	public void setAreas(int[][] areas) {
		this.areas = areas;
	}

	public int[][] getSolutionBoard() {
		return solutionBoard;
	}


}
