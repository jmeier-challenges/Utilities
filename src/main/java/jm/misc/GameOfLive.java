package jm.misc;

import java.util.Arrays;

/**
 * User: T05365A
 * Date: 01.04.11
 * Time: 14:27
 *
 * x=8, y=8
 *
 * http://golly.sourceforge.net/
 */
public class GameOfLive {
	private boolean[][] world;
	private boolean[][] nextWorld;
	private int width;
	private int height;
	private int countGeneration = 0;

	public void set(final int x, final int y, final boolean b) {
		set(x, y, b, world);
	}

	public int getLivingCells() {
		return getLivingCells(world);
	}

	private static int getLivingCells(final boolean[][] w1) {
		int count = 0;
		for (int y = 0; y < w1.length; y++) {
			for (int x = 0; x < w1[y].length; x++) {
				if (w1[y][x]) {
					count++;
				}
			}
		}
		return count;
	}

	private void setSize(final int size) {
		world = new boolean[size][size];
		nextWorld = new boolean[size][size];
		width = size;
		height = size;
	}

	public void nextGeneration() {
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				set(x, y, isAlive(x, y), nextWorld);
			}
		}
		copyNextToCurrentWorld();
		countGeneration++;
	}

	private boolean isAlive(final int x, final int y) {
		int numberOfLiveNeighbours = getNeighbours(x, y);
		return (numberOfLiveNeighbours == 2 && get(x, y, world))|| numberOfLiveNeighbours == 3 ;
	}

	private int getNeighbours(final int x, final int y) {
		int count = 0;
		if (isNeighbourAlive(x-1, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x-1, y)) {
			count++;
		}
		if(isNeighbourAlive(x-1, y+1)) {
			count++;
		}
		if(isNeighbourAlive(x, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x, y+1)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y+1)) {
			count++;
		}
		return count;
	}

	private boolean isNeighbourAlive(final int x, final int y) {
		return get(x, y, world);
	}

	private void copyNextToCurrentWorld() {
		/*
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				set(x, y, get(x, y, nextWorld) , world) ;
			}
		}
		*/
		boolean[][] tmpWorld = world;
		world = nextWorld;
		nextWorld = tmpWorld;
	}

	private void set(int x, int y, boolean b, boolean[][] world) {
		world[y][x] = b;
	}

	private boolean get(int x, int y, boolean[][] world) {
		if (x < 0 || x >= width || y < 0  || y >= height) {
			return false;
		} else {
			return world[y][x];
		}
	}

	public boolean[][] getWorld() {
		boolean result[][] = new boolean[world.length][world[0].length];
		for (int y = 0; y < world.length; y++) {
			result[y] = world[y].clone();
		}
		return result;
	}


	public int getCountGeneration() {
		return countGeneration;
	}

	public void setWorld(boolean[][] newWorld) {
		int rowCount = newWorld.length;
		if (rowCount > 0 && newWorld[0].length > 0) {
			world = new boolean[rowCount][];
			for (int row = 0; row < rowCount; row++) {
				world[row] = Arrays.copyOf(newWorld[row], newWorld[row].length );
			}
			width = newWorld[0].length;
			height = rowCount;
			nextWorld = new boolean[height][width];
		} else {
			throw new RuntimeException("World must not be empty");
		}
	}

	public String toString() {
		return toString(world);
	}

	public String toString(boolean[][] map) {
		int width = map[0].length;
		int height = map.length;
		StringBuilder result = new StringBuilder();
		result.append(' ');

		for (int i = 0; i < width; i++) {
			result.append(i % 10);
		}
		result.append('\n');

		for (int y = 0; y < height; y++) {
			result.append(y % 10);
			for (int x = 0; x < width; x++) {
				result.append(get(x, y, map) ? '*' : ' ');
			}
			result.append(y%height);
			result.append("\n");
		}

		result.append(' ');
		for (int i = 0; i < width; i++) {
			result.append(i%10);
		}
		result.append('\n');
		return result.toString();
	}
}