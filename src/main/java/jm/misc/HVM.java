package jm.misc;

import java.util.Stack;

/**
 * User: T05365A
 * Date: 16.01.13
 * Time: 12:04
 */

/*
Hack VM - A Virtual Machine for Hackers
The Hack VM is a tiny, trivial, virtual machine. Its purpose is to be used as a simple execution engine that can run very simple programs. Some of the challenges, for example, require you to write programs for this virtual machine that produce a certain result.

The virtual machine executes a single program and terminates, either by reaching the end of the code, an '!' instruction, or because an exception was thrown during execution. A program is represented by a string of single-character instructions. The virtual machine starts with the first instruction, executes it, and moves on to the next instruction, etc... The index of the current instruction is called the program counter. The execution model is simple: the virtual machine has an operand stack, a memory buffer, and a call stack. Each item on the operand stack or in memory is a cell that can hold a signed integer. For implementation reasons, those integers are currently limited to 32 bits, but do not count on it, they could be large in future implementations. The call stack is used to push the value of the program counter when jumping to a routine from which we want to return.

The memory buffer holds 16384 cells. The first cell has address 0, the next one address 1, etc...

By default, the execution starts with and empty call stack, empty operand stack and all the cells in the memory buffer set to 0. When specified, it is possible for the memory buffer to have been initialized to a known state before executing a program.

Instructions
In the following descriptions, S<n> is the (n+1)th value from the top of the stack (S0 is at the top, then S1, S2, etc...) Instruction Description
' ' Do Nothing
'p' Print S0 interpreted as an integer
'P' Print S0 interpreted as an ASCII character (only the least significant 7 bits of the value are used)
'0' Push the value 0 on the stack
'1' Push the value 1 on the stack
'2' Push the value 2 on the stack
'3' Push the value 3 on the stack
'4' Push the value 4 on the stack
'5' Push the value 5 on the stack
'6' Push the value 6 on the stack
'7' Push the value 7 on the stack
'8' Push the value 8 on the stack
'9' Push the value 9 on the stack
'+' Push S1+S0
'-' Push S1-S0
'*' Push S1*S0
'/' Push S1/S0
':' Push -1 if S1<S0, 0 if S1=S0, or 1 S1>S0
'g' Add S0 to the program counter
'?' Add S0 to the program counter if S1 is 0
'c' Push the program counter on the call stack and set the program counter to S0
'$' Set the program counter to the value pop'ed from the call stack
'<' Push the value of memory cell S0
'>' Store S1 into memory cell S0
'^' Push a copy of S<S0+1> (ex: 0^ duplicates S0)
'v' Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
'd' Drop S0
'!' Terminate the program

Examples
Input: "78*p" Output: 56Input: "123451^2v5:4?9p2g8pppppp" Output: 945321
 */
public class HVM {
	// Constants
	int MAX_CYCLES = 10000;

	private int[] memory = new int[16384];

	private Stack<Integer> callStack = new Stack<Integer>();

	private Stack<Integer> operandStack = new Stack<Integer>();

	private int programCounter = 0;

	private void interprete(char instruction) {
		switch (instruction) {
			case '\n':
			case ' ': // Do Nothing
				doNothing();
				break;
			case 'p': // Print S0 interpreted as an integer
				doPrintInt();
				break;
			case 'P': // Print S0 interpreted as an ASCII character (only the least significant 7 bits of the value are used)
				doPrintChar();
				break;
			case '0': // Push the value 0 on the stack
				push(0);
				break;
			case '1': // Push the value 1 on the stack
				push(1);
				break;
			case '2': // Push the value 2 on the stack
				push(2);
				break;
			case '3':// Push the value 3 on the stack
				push(3);
				break;
			case '4': // Push the value 4 on the stack
				push(4);
				break;
			case '5': // Push the value 5 on the stack
				push(5);
				break;
			case '6': // Push the value 6 on the stack
				push(6);
				break;
			case '7': // Push the value 7 on the stack
				push(7);
				break;
			case '8': // Push the value 8 on the stack
				push(8);
				break;
			case '9': // Push the value 9 on the stack
				push(9);
				break;
			case '+': // Push S1+S0
				doAdd();
				break;
			case '-': // Push S1-S0
				doSub();
				break;
			case '*': // Push S1*S0
				doMul();
				break;
			case '/': // Push S1/S0
				doDiv();
				break;
			case ':': // Push -1 if S1<S0, 0 if S1=S0, or 1 S1>S0
				doCmp();
				break;
			case 'g': //Add S0 to the program counter
				doGoto();
				break;
			case '?': //Add S0 to the program counter if S1 is 0
				doGotoIfZero();
				break;
			case 'c': // Push the program counter on the call stack and set the program counter to S0
				doCall();
				break;
			case '$': // Set the program counter to the value pop'ed from the call stack
				doReturn();
				break;
			case '<': // Push the value of memory cell S0
				doPeek();
				break;
			case '>': // Store S1 into memory cell S0
				doPoke();
				break;
			case '^': // Push a copy of S<S0+1> (ex: 0^ duplicates S0)
				doPick();
				break;
			case 'v': // Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
				doRoll();
				break;
			case 'd': // Drop S0
				operandStack.pop();
				break;
			case '!': // Terminate the program
				System.exit(0);
				break;
		}
	}
	private void doNothing() {
		// do nothing
	}

	private void push(Integer i) {
		operandStack.push(i);
	}

	private Integer pop() {
		return operandStack.pop();
	}

	private void doPrintChar() {
		System.out.print((char) (pop() & 0x7F));
	}

	private void doPrintInt() {
		System.out.print(pop());
	}

	private void doAdd() {
		push(pop() + pop());
	}

	private void doSub() {
		Integer a = pop();
		Integer b = pop();
		push(b - a);
	}

	private void doMul() {
		push(pop() * pop());
	}

	private void doDiv() {
		Integer a = pop();
		Integer b = pop();
		push(b / a);
	}

	private void doCmp() {
		Integer a = pop();
		Integer b = pop();
		push(b.compareTo(a));
	}

	private void doGoto() {
		programCounter += pop();
	}

	private void doGotoIfZero() {
		Integer offset = pop();
		if (pop() == 0) {
			programCounter += offset;
		}
	}

	private void doCall() {
		callStack.push(programCounter);
		programCounter = pop();
	}

	private void doReturn() {
		programCounter = callStack.pop();
	}

	private void doPeek() {
		Integer addr = pop();
		if (addr < 0 || addr >= memory.length) {
			throw new RuntimeException("memory read access violation @(" + programCounter + ", " + addr + ")");
		}
		push(memory[addr]);
	}

	private void doPoke() {
		Integer addr = pop();
		if (addr < 0 || addr >= memory.length) {
			throw new RuntimeException("memory read access violation @(" + programCounter + ", " + addr + ")");
		}
		memory[addr] = pop();
	}

	private void doPick() {
		// Push a copy of S<S0+1> (ex: 0^ duplicates S0)
		Integer where = operandStack.pop();
		if (where >= 0 && where < operandStack.size()) {
			int pos = operandStack.size() - 1 - where;
			operandStack.push(operandStack.get(pos));
		} else {
			throw new RuntimeException("out of stack bounds @" + programCounter);
		}
	}

	private void doRoll() {
		// Remove S<S0+1> from the stack and push it on top (ex: 1v swaps S0 and S1)
		Integer where = operandStack.pop();
		if (where >= 0 && where < operandStack.size()) {
			int pos = operandStack.size() - 1 - where;
			Integer v = operandStack.remove(pos);
			operandStack.push(v);
		} else {
			throw new RuntimeException("out of stack bounds @" + programCounter);
		}
	}

	private void run(String code) {
		int cycleCounter = 0;
		programCounter = 0;

		char op_code = ' ';
		try {
			while (programCounter != code.length()) {
				op_code = code.charAt(programCounter);
				programCounter++;
				//cycleCounter++;
				if (cycleCounter > MAX_CYCLES) {
					throw new RuntimeException("to many cycles");
				}
				interprete(op_code);
				if (programCounter < 0 || programCounter > code.length()) {
					throw new RuntimeException("out of code bounds");
				}
			}
		} catch (RuntimeException ex) {
			System.err.println("!Error: exception while executing I='" + op_code + "' PC=" + (programCounter - 1) + " STACK_SIZE=" + operandStack.size());
			System.err.println(ex);
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		HVM hvm = new HVM();
		hvm.memory[0] = 9;
		hvm.memory[1] = 8;
		String program = "0<1<:1:5?0<p!d1<p";
		hvm.run(program);
	}
}
