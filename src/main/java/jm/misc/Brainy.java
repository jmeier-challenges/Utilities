package jm.misc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: T05365A
 * Date: 14.01.13
 * Time: 15:26
 */
public class Brainy {
	private int[] memory = new int[10000];
	private String program;
	private int pointer = 0;
	private int programPointer = 0;
	private List<Integer> loops = new ArrayList<Integer>();
	private StringBuilder errors = new StringBuilder();
	boolean debug = false;

	private void interprete(char command) throws IOException {
		switch (command) {
			case '>':
				++pointer;
				break;
			case '<':
				if (pointer == 0) {
					errors.append(String.format("(%d,%d) pointer already 0\n", programPointer, pointer));
				} else {
					--pointer;
				}
				break;
			case '+':
				memory[pointer]++;
				break;
			case '-':
				if (memory[pointer] == 0) {
					errors.append(String.format("(%d,%d) memory already 0\n", programPointer, pointer));
				} else {
					memory[pointer]--;
				}
				break;
			case '.':
				putChar();
				break;
			case ',':
				readChar();
				break;
			case '[':
				if (memory[pointer] == 0) {
					gotoEndOfLoop();
				} else {
					pushLoop();
				}
				break;
			case ']':
				if (memory[pointer] != 0) {
					programPointer = popLoop();
				} else {
					popLoop();
				}
				break;
			case '#':
				int breakpoint = 0;
				break;

		}
	}

	private void gotoEndOfLoop() {
		int loop = 1;
		do {
			char p = program.charAt(++programPointer);
			if (p == '[') {
				loop++;
			}
			if (p == ']') {
				loop--;
			}
		} while (loop > 0);
	}

	private int popLoop() {
		return loops.remove(loops.size() - 1);
	}

	private void pushLoop() {
		loops.add(programPointer - 1);
	}

	private void readChar() throws IOException {
		memory[pointer] = System.in.read();
	}

	private void putChar() {
		System.out.print((char)memory[pointer]);
	}

	public void run(String p) throws IOException {
		debug = true;
		program = p;
		programPointer = -1;
		pointer = 0;
		do {
			programPointer++;
			interprete(program.charAt(programPointer));
		} while (programPointer < program.length() - 1);
		if (debug) {
			System.err.println();
			System.err.println(errors);
			System.err.println(errors.length());
		}
	}

	public static void main(String[] args) throws IOException {
		String prog = "++++++++++[>+++++++>++++++++++>+++>+<<<<-]>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.+++.";
		System.out.println("bR3al");
		prog = ">++++++++++[<++++++++>-]<+++++." +
			   ">++++++[<+++++>-]<." +
			   ">++[<------->-]<." +
			   ">+++[<++++>-]<+." +
			   "----." +
			   ">+++[<---->-]<-." +
			   ">+++[<++++>-]<." +
			   ">++[<---->-]<." +
			   ">++++[<---------->-]<---." +
			   ">++++[<++++++++++>-]<++" +
			   ">>++++++++++[<++++++++>-]<+" +
			   ">>++++++++++[<+++++>-]<+++" +
			   ">>++++++++++[<++++++++++>-]<-------" +
			   ">>+++++++++++[<++++++++++>-]<+++" +
			   ">,++." +
			   ">,." +
			   ">,." +
			   ">,----." +
			   ">,." +
			   "#>>>++++++++[<++++>-]<>>++++++++++[<+++++++++>-]<--->>+++++++++++[<++++++++++>-]<++++>"
			+ ">+++++++++++[<++++++++++>-]<+>>+++++++++++[<++++++++++>-]<>>++++++++++[<++++++++++>-]"
			+ "<+++>>++++++[<+++++>-]<+++>+>>++++++++[<++++>-]<>>++++++++++[<+++++++>-]<--->>+++++++++++[<++++++++++>-]"
			+ " <+>>+++++++++++[<++++++++++>-]<++++>>+++++++++++[<++++++++++>-]<++++>>++++++++++[<++++++++++>-]"
			+ "<+>>++++++++++[<++++++++++>-]<->>+++++++++++[<++++++++++>-]<++++++>>++++++[<+++++>-]<+++<<<<<<<<<<<<<<<<<<<<<<"
			+ "[<<<<<->>>>>-]<<<<<[>>>>>>>>>>+<<<<<<<<<<[-]]>>>>>>-[<<<<<->>>>>-]<<<<<[>>>>>>>>>+<<<<<<<<<[-]]"
			+ ">>>>>>++[<<<<<->>>>>-]<<<<<[>>>>>>>>+<<<<<<<<[-]]>>>>>>[<<<<<->>>>>-]<<<<<[>>>>>>>+<<<<<<<[-]]>>>>>>+++++"
			+ "[<<<<<->>>>>-]<<<<<[>>>>>>+<<<<<<[-]]>>>>>>[>.>.>.>.>.>.>.>-<<<<<<<<[-]]>>>>>>>>[>.>.>.>.>.>.>.>.>.<<<<<<<<<[-]]";
		//prog = "[-]++[>++[>+<-]<<-]>>.";

		Brainy brainy = new Brainy();
		brainy.run(prog);

	}
}
