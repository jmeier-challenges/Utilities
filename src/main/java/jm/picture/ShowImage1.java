package jm.picture;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ShowImage1 extends Panel {
	BufferedImage image;

	public ShowImage1() {
		try {
			String imageName = "c:/wsc/Challenges/mona_lisa.png";
			File input = new File(imageName);
			image = ImageIO.read(input);
		} catch (IOException ie) {
			System.out.println("Error:" + ie.getMessage());
		}
	}

	public void paint(Graphics g) {
		g.drawImage(image, 0, 0, null);
	}

	static public void main(String args[]) throws Exception {
		JFrame frame = new JFrame("Display image");
		Panel panel = new ShowImage1();
		frame.getContentPane().add(panel);
		frame.setSize(500, 500);
		frame.setVisible(true);
	}
}