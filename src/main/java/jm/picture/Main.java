package jm.picture;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main extends JFrame {
	public Main() throws IOException {
		BufferedImage image = ImageIO.read(new File("c:/wsc/Challenges/mona_lisa.png"));
		setSize(500, 500);
		JPanel panel = new JPanel();
		ImageIcon icon = new ImageIcon(image);
		JLabel label = new JLabel(icon);
		panel.add(label);
		this.getContentPane().add(new JScrollPane(panel));

		setVisible(true);
	}

	public static void main(String[] args) throws IOException // no args expected
	{
		new Main();
	} // end main
} // end class ImageDisplay

