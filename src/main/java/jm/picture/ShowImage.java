package jm.picture;
/**
 * User: T05365A
 * Date: 07.11.12
 * Time: 18:00
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

public class ShowImage extends JFrame implements KeyListener, MouseWheelListener {
    private BufferedImage image;
    private int scale = 1;
    private ImagePanel panel;

    public ShowImage(BufferedImage bufferedImage) {
        this.image = bufferedImage;
        int width = image.getWidth();
        int height = image.getHeight();
        setTitle("Show Image");
        showImage(image);
        setSize(width, height);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        addKeyListener(this);
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyChar() == '+') {
            scaleUp();
        } else if (e.getKeyChar() == '-') {
            scaleDown();
        }
    }

    private void scaleDown() {
        int width = image.getWidth();
        int height = image.getHeight();
        if (scale > 0) {
            scale--;
        }

        if (scale == 0) {
            showImage(image);
        } else {
            showImage(image.getScaledInstance(width * scale, height * scale, BufferedImage.SCALE_FAST));
            panel.scaleDown();
        }
    }

    private void showImage(final Image image) {
        if (panel == null) {
            panel = new ImagePanel();
            panel.setMaximumSize(new Dimension(1000, 800));
            JScrollPane scrollPane = new JScrollPane(panel);
            scrollPane.addMouseWheelListener(this);
            getContentPane().add(scrollPane);
        }

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                int width = image.getWidth(null);
                int height = image.getHeight(null);
                panel.setImage(image);
                if (width <= panel.getMaximumSize().width && height <= panel.getMaximumSize().height) {
                    panel.setPreferredSize(new Dimension(width, height));
                    pack();
                }
                validate();
                repaint();
            }
        });
    }

    class ImagePanel extends JPanel {
        Image image;
        int offsetX = 0;
        int offsetY = 0;
        Point mousePressedPoint;
        Point mouseReleasedPoint;

        ImagePanel() {
            this.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseReleased(MouseEvent e) {
                    mouseReleasedPoint = e.getPoint();
                    setOffsetX(offsetX + getDiffX());
                    setOffsetY(offsetY + getDiffY());
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            validate();
                            repaint();
                        }
                    });
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    mousePressedPoint = e.getPoint();
                }
            });
        }

        private boolean isOnPanel(Point point) {
            return point.x >= 0 && point.x < getWidth() * scale && point.y >= 0 && point.y < getHeight() * scale;
        }

        private Point getCurrentMousePosition() {
            Point position = MouseInfo.getPointerInfo().getLocation();
            Point locationOnScreen = getLocationOnScreen();
            return new Point(position.x - locationOnScreen.x, position.y - locationOnScreen.y);
        }

        private int getDiffX() {
            return mouseReleasedPoint.x - mousePressedPoint.x;
        }

        private int getDiffY() {
            return mouseReleasedPoint.y - mousePressedPoint.y;
        }

        public void setImage(Image image) {
            this.image = image;
        }

        public void setOffsetX(int offsetX) {
            this.offsetX = offsetX;
        }

        public void setOffsetY(int offsetY) {
            this.offsetY = offsetY;
        }

        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(image, offsetX, offsetY, null);
        }

        public void scaleUp() {
            Point mousePosition = getCurrentMousePosition();
            if (isOnPanel(mousePosition)) {
                offsetX -= mousePosition.x;
                offsetY -= mousePosition.y;
            }
        }

        public void scaleDown() {
            Point mousePosition = getCurrentMousePosition();
            if (isOnPanel(mousePosition)) {
                offsetX += mousePosition.x;
                offsetY += mousePosition.y;
            }
        }

    }

    private void scaleUp() {
        panel.scaleUp();
        int width = image.getWidth();
        int height = image.getHeight();
        scale++;
        showImage(image.getScaledInstance(width * scale, height * scale, BufferedImage.SCALE_FAST));
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        int notches = e.getWheelRotation();
        if (notches < 0) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    scaleUp();
                }
            });
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    scaleDown();
                }
            });
        }
    }
}
