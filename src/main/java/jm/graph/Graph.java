package jm.graph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: johann
 * Date: 23.04.11
 * Time: 12:35
 */
public class Graph {
    private Map<Knoten, List<Knoten>> adjazenzListen = new HashMap<Knoten, List<Knoten>>();
    private  boolean[] besucht;
    public void addKnoten(Knoten k, List<Knoten> nachbarn) {
        adjazenzListen.put(k, nachbarn);
    }

    public void dfs(Knoten root) {
        initBesucht();
        dfsRec(root);

    }

    private void dfsRec(Knoten root) {
        if (!besucht[root.getId()]) {
            System.out.println(root);
            besucht[root.getId()] = true;
            List<Knoten> nachbarn = adjazenzListen.get(root);
            for (Knoten knoten : nachbarn) {
                dfsRec(knoten);
            }
        }
    }

    private void initBesucht() {
        besucht = new boolean[adjazenzListen.size()];
        for (int i = 0; i < adjazenzListen.size(); i++) {
            besucht[i] = false;
        }

    }

    public void dijkstra(Knoten k) {

    }
}
