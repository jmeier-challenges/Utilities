package jm.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * User: johann
 * Date: 23.04.11
 * Time: 12:46
 */
public class GraphTest {
    private Graph g;
    private Knoten k1 = new Knoten(0);
    private Knoten k2 = new Knoten(1);
    private Knoten k3 = new Knoten(2);
    private Knoten k4 = new Knoten(3);
    private Knoten k5 = new Knoten(4);

    public static void main(String[] args) {
        GraphTest gt = new GraphTest();
        //gt.ungewichtet();
        gt.gewichtet();
    }

    private void gewichtet() {
        g = new Graph();
        addKnotenA();
        addKnotenB();
        addKnotenC();
        addKnotenD();
        addKnotenE();
        addKnotenF();
        g.dfs(new Knoten(getId('A')));
    }

    private void ungewichtet() {
        g = new Graph();
        addKnoten1();
        addKnoten2();
        addKnoten3();
        addKnoten4();
        addKnoten5();

        g.dfs(k1);
        g.dfs(k2);
    }

    private void addKnotenA() {
        Knoten k = new Knoten(getId('A'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(new Knoten(getId('B'), 30));
        nachbarn.add(new Knoten(getId('E'), 100));
        nachbarn.add(new Knoten(getId('F'), 90));

        g.addKnoten(k, nachbarn);

    }
    private void addKnotenB() {
        Knoten k = new Knoten(getId('B'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(new Knoten(getId('C'), 10));
        nachbarn.add(new Knoten(getId('D'), 40));

        g.addKnoten(k, nachbarn);

    }

    private void addKnotenC() {
        Knoten k = new Knoten(getId('C'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(new Knoten(getId('A'), 40));
        nachbarn.add(new Knoten(getId('F'), 10));

        g.addKnoten(k, nachbarn);

    }

    private void addKnotenD() {
        Knoten k = new Knoten(getId('D'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(new Knoten(getId('E'), 30));

        g.addKnoten(k, nachbarn);

    }

    private void addKnotenE() {
        Knoten k = new Knoten(getId('E'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();

        g.addKnoten(k, nachbarn);

    }

    private void addKnotenF() {
        Knoten k = new Knoten(getId('F'));
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(new Knoten(getId('E'), 20));

        g.addKnoten(k, nachbarn);

    }

    private int getId(char b) {
        return b - 'A';
    }

    private void addKnoten1() {
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(k2);
        nachbarn.add(k4);

        g.addKnoten(k1, nachbarn);
    }

    private void addKnoten2() {
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(k1);
        nachbarn.add(k4);

        g.addKnoten(k2, nachbarn);
    }

    private void addKnoten3() {
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(k3);
        nachbarn.add(k5);

        g.addKnoten(k3, nachbarn);
    }

    private void addKnoten4() {
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(k5);

        g.addKnoten(k4, nachbarn);
    }

    private void addKnoten5() {
        List<Knoten> nachbarn = new ArrayList<Knoten>();
        nachbarn.add(k3);

        g.addKnoten(k5, nachbarn);
    }
}
