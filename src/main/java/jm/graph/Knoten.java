package jm.graph;

/**
 * User: johann
 * Date: 23.04.11
 * Time: 12:33
 */
public class Knoten {
    private int id;

    private int gewicht;

    public Knoten(int id) {
        this.id = id;
        gewicht = 0;
    }

    public Knoten(int id, int gewicht) {
        this.id  = id;
        this.gewicht = gewicht;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Knoten knoten = (Knoten) o;

        if (id != knoten.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    public int getId() {
        return id;
    }

    public int getGewicht() {
        return gewicht;
    }

    public void setGewicht(int gewicht) {
        this.gewicht = gewicht;
    }

    @Override
    public String toString() {
        return "Knoten{" +
                "id=" + (id+1) +
                ", gewicht=" + gewicht +
                '}';
    }
}
