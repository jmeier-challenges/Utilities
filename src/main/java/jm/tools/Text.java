package jm.tools;

import java.util.*;

public class Text {

    public final static String LARGE_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    public final static String LC_ALPHABET = "abcdefghijklmnopqrstuvwxyz";
    public static final String UC_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public final static String[] ALPHABET_ARRAY = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
    public final static String[] COMMON_ALPHABETS = {LC_ALPHABET, UC_ALPHABET, LARGE_ALPHABET};


    // http://www.mathe.tu-freiberg.de/~hebisch/cafe/kryptographie/bigramme.html
	private static final int[][] bigramFrequencyDE =
			{{8, 31, 27, 11, 64, 15, 30, 20, 5, 1, 7, 59, 28, 102, 0, 4, 0, 51, 53, 46, 75, 2, 3, 0, 1, 2},
			{16, 1, 0, 1, 101, 0, 3, 1, 12, 0, 1, 9, 0, 1, 8, 0, 0, 9, 6, 4, 14, 0, 1, 0, 1, 1},
			{2, 0, 0, 2, 1, 0, 0, 242, 1, 0, 14, 1, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
			{54, 3, 1, 13, 227, 3, 4, 2, 93, 1, 3, 5, 4, 6, 9, 3, 0, 10, 11, 6, 16, 3, 4, 0, 0, 3},
			{26, 45, 25, 51, 23, 26, 50, 57, 193, 3, 19, 63, 55, 400, 6, 13, 1, 409, 140, 55, 36, 14, 23, 2, 1, 11},
			{19, 2, 0, 9, 25, 12, 3, 1, 7, 0, 1, 5, 1, 2, 9, 1, 0, 18, 4, 20, 24, 1, 1, 0, 0, 1},
			{20, 3, 0, 12, 147, 2, 3, 3, 19, 1, 3, 9, 3, 5, 6, 1, 0, 14, 18, 18, 11, 4, 3, 0, 0, 3},
			{70, 4, 1, 14, 102, 2, 4, 3, 23, 1, 3, 25, 11, 19, 18, 1, 0, 37, 11, 47, 11, 4, 9, 0, 0, 3},
			{7, 7, 76, 20, 163, 5, 38, 12, 1, 1, 12, 25, 27, 168, 20, 2, 0, 17, 79, 78, 3, 5, 1, 0, 0, 5},
			{7, 0, 0, 0, 9, 5, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0},
			{28, 1, 0, 2, 26, 1, 1, 1, 7, 0, 1, 10, 1, 1, 24, 1, 0, 13, 5, 14, 9, 1, 1, 0, 0, 1},
			{45, 7, 2, 14, 65, 5, 6, 2, 61, 1, 7, 42, 3, 4, 14, 2, 0, 2, 22, 27, 13, 3, 2, 0, 0, 3},
			{40, 6, 1, 8, 50, 4, 4, 3, 44, 2, 3, 4, 23, 3, 15, 7, 0, 2, 10, 8, 14, 4, 3, 0, 0, 2},
			{68, 23, 5, 187, 122, 19, 94, 17, 65, 5, 25, 10, 23, 43, 18, 10, 0, 10, 74, 59, 33, 18, 29, 0, 0, 25},
			{3, 8, 15, 7, 25, 6, 5, 9, 1, 1, 3, 31, 17, 64, 1, 6, 0, 50, 19, 9, 3, 3, 7, 0, 1, 6},
			{16, 0, 0, 3, 10, 6, 0, 2, 4, 0, 0, 4, 0, 0, 11, 5, 0, 23, 1, 3, 4, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0},
			{80, 25, 9, 67, 112, 18, 27, 19, 52, 4, 23, 18, 20, 31, 30, 9, 0, 15, 54, 49, 48, 12, 17, 0, 0, 14},
			{36, 10, 89, 20, 99, 7, 13, 9, 65, 2, 11, 9, 12, 7, 28, 22, 0, 8, 76, 116, 15, 9, 10, 0, 2, 7},
			{57, 8, 1, 35, 185, 5, 10, 14, 59, 2, 4, 11, 9, 9, 15, 3, 0, 31, 50, 23, 26, 8, 21, 0, 1, 26},
			{3, 8, 16, 5, 78, 27, 8, 4, 2, 0, 3, 7, 21, 119, 0, 5, 0, 33, 48, 23, 1, 3, 2, 0, 0, 1},
			{3, 0, 0, 0, 37, 0, 0, 0, 9, 0, 0, 0, 0, 0, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{34, 0, 0, 0, 48, 0, 0, 0, 36, 1, 0, 0, 0, 1, 17, 0, 0, 0, 1, 0, 9, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
			{4, 1, 0, 1, 28, 0, 1, 0, 11, 0, 1, 2, 1, 0, 2, 0, 0, 0, 1, 7, 43, 1, 9, 0, 0, 1}};

	private static final int[][] bigramFrequencyEN =
			{{1, 32, 39, 15, 0, 10, 18, 0, 16, 0, 10, 77, 18, 172, 2, 31, 1, 101, 67, 124, 12, 24, 7, 0, 27, 1},
			{8, 0, 0, 0, 58, 0, 0, 0, 6, 2, 0, 21, 1, 0, 11, 0, 0, 6, 5, 0, 25, 0, 0, 0, 19, 0},
			{44, 0, 12, 0, 55, 1, 0, 46, 15, 0, 8, 16, 0, 0, 59, 1, 0, 7, 1, 38, 16, 0, 1, 0, 0, 0},
			{45, 18, 4, 10, 39, 12, 2, 3, 57, 1, 0, 7, 9, 5, 37, 7, 1, 10, 32, 39, 8, 4, 9, 0, 6, 0},
			{65, 11, 64, 107, 39, 23, 20, 15, 40, 1, 2, 46, 43, 120, 46, 32, 14, 154, 145, 80, 7, 16, 41, 17, 17, 0},
			{21, 2, 9, 1, 25, 14, 1, 6, 21, 1, 0, 10, 3, 2, 38, 3, 0, 4, 8, 42, 11, 1, 4, 0, 1, 0},
			{11, 2, 1, 1, 32, 3, 1, 16, 10, 0, 0, 4, 1, 3, 23, 1, 0, 21, 7, 13, 8, 0, 2, 0, 1, 0},
			{84, 1, 2, 1, 251, 2, 0, 5, 72, 0, 0, 3, 1, 2, 46, 1, 0, 8, 3, 22, 2, 0, 7, 0, 1, 0},
			{18, 7, 55, 16, 37, 27, 10, 0, 0, 0, 8, 39, 32, 169, 63, 3, 0, 21, 106, 88, 0, 14, 1, 1, 0, 4},
			{0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0},
			{0, 0, 0, 0, 28, 0, 0, 0, 8, 0, 0, 0, 0, 3, 3, 0, 0, 0, 2, 1, 0, 0, 3, 0, 3, 0},
			{34, 7, 8, 28, 72, 5, 1, 0, 57, 1, 3, 55, 4, 1, 28, 2, 2, 2, 12, 19, 8, 2, 5, 0, 47, 0},
			{56, 9, 1, 2, 48, 0, 0, 1, 26, 0, 0, 0, 5, 3, 28, 16, 0, 0, 6, 6, 13, 0, 2, 0, 3, 0},
			{54, 7, 31, 118, 64, 8, 75, 9, 37, 3, 3, 10, 7, 9, 65, 7, 0, 5, 51, 110, 12, 4, 15, 1, 14, 0},
			{9, 18, 18, 16, 3, 94, 3, 3, 13, 0, 5, 17, 44, 145, 23, 29, 0, 113, 37, 53, 96, 13, 36, 0, 4, 2},
			{21, 1, 0, 0, 40, 0, 0, 7, 8, 0, 0, 29, 0, 0, 28, 26, 42, 3, 14, 7, 0, 1, 0, 2, 0, 0},
			{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0},
			{57, 4, 14, 16, 148, 6, 6, 3, 77, 1, 11, 12, 15, 12, 54, 8, 0, 18, 39, 63, 6, 5, 10, 0, 17, 0},
			{75, 13, 21, 6, 84, 13, 6, 30, 42, 0, 2, 6, 14, 19, 71, 24, 2, 6, 41, 121, 30, 2, 27, 0, 4, 0},
			{56, 14, 6, 9, 94, 5, 1, 315, 128, 0, 0, 12, 14, 8, 111, 8, 0, 30, 32, 53, 22, 4, 16, 0, 21, 0},
			{18, 5, 17, 11, 11, 1, 12, 2, 5, 0, 0, 28, 9, 33, 2, 17, 0, 49, 42, 45, 0, 0, 0, 1, 1, 1},
			{15, 0, 0, 0, 53, 0, 0, 0, 19, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			{32, 0, 3, 4, 30, 1, 0, 48, 37, 0, 0, 4, 1, 10, 17, 2, 0, 1, 3, 6, 1, 1, 2, 0, 0, 0},
			{3, 0, 5, 0, 1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0},
			{11, 11, 10, 4, 12, 3, 5, 5, 18, 0, 0, 6, 4, 3, 28, 7, 0, 5, 17, 21, 1, 3, 14, 0, 0, 0},
			{0, 0, 0, 0, 5, 0, 0, 0, 2, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};

    private String alphabet;

    public Text(String alphabet) {
        this.alphabet = alphabet;
    }
    public Text() {
        this.alphabet = LARGE_ALPHABET;
    }

    public static Map<String, Integer> countWords(String text) {
		Map<String, Integer> wordcounts = new HashMap<String, Integer>();

		String word = "";
		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			if (Character.isLetter(c)) {
				 word += c;
			} else {
				int count = 1;
				if (wordcounts.containsKey(word)) {
					count = wordcounts.get(word) + 1;
				}
				wordcounts.put(word, count);
				word = "";
			}
		}

		return wordcounts;
	}
	/*
	Deutsch : enisrat
	Englisch: etaoins
	 */
	public static Map<Character, Integer> countChars(String text) {
		Map<Character, Integer> charCounts = new HashMap<Character, Integer>();

		for (int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			int count = 1;
			if (charCounts.containsKey(c)) {
				count = charCounts.get(c) + 1;
			}
			charCounts.put(c, count);
		}

		return charCounts;
	}

	public String transform1ToAbc(String message) {
		StringBuffer result = new StringBuffer();
		int currentPos = 0;
		Map<Character, Character> charTransformation = new HashMap<Character, Character>();
		charTransformation.put(' ', ' ');

		for (int i = 0; i < message.length(); i++) {
			char c = message.charAt(i);
			if (!charTransformation.containsKey(c)) {
				charTransformation.put(c, alphabet.charAt(currentPos++));
			}
			result.append(charTransformation.get(c));
		}
		return result.toString();
	}

	public String transform2ToAbc(String message, String ignore) {
		StringBuffer result = new StringBuffer();
		int currentPos = 0;
		Map<String, Character> charTransformation = new HashMap<String, Character>();
		String s = "";
		int pos = 0;
		for (int i = 0; i < message.length(); i++) {
			char c = message.charAt(i);
			int index = ignore.indexOf(c);
			if (index >= 0) {
				result.append(c);
			} else  if (pos == 0) {
				s = "";
				s += c;
				pos++;
			} else {
				pos = 0;
				s += c;
				if (!charTransformation.containsKey(s)) {
					charTransformation.put(s, alphabet.charAt(currentPos++));
				}
				result.append(charTransformation.get(s));
			}
		}
		if (pos == 1) {
			result.append(s);
		}
		return result.toString();
	}

	public String transform2ToAbc(String message) {
		return transform2ToAbc(message, "");
	}


	/*
		Franzoesisch 0.0778
		Spanisch 0.0775
		Italienisch 0.0738
		Russisch 0.0529
		Englisch 0.0667
		Deutsch 0.0762
	 */
	public static  float getKoinzidenzIndex(String text) {
		int[] n = getCharacterFrequencies(text);
		int sum = 0;
		int count = 0;
		for (int i : n) {
			sum += i * (i - 1);
			count += i;
		}
		//System.out.println("sum " + sum);
		//System.out.println("count " + count);
		return (float)sum / (count * (count - 1));
	}

	public static int[] getCharacterFrequencies(final String text) {
		int[] frequencies = new int[26];
		for (int i = 0; i < frequencies.length; i++) {
			frequencies[i] = 0;
		}
		for (int i = 0; i < text.length(); i++) {
			char c = Character.toLowerCase(text.charAt(i));
			if (c >= 'a' && c <='z') {
				frequencies[c - 'a']++;
			}
			if (isUmlaut(c)) {
				char[] u = getUmlautChars(c);
				for (char c1 : u) {
					frequencies[c1 - 'a']++;
				}
			}
		}
		return frequencies;
	}

	private static char[] getUmlautChars(final char c) {
		if (c == 'ä') {
			return new char[]{'a', 'e'};
		}
		if (c == 'Ä') {
			return new char[]{'A', 'e'};
		}
		if (c == 'ö') {
			return new char[]{'o', 'e'};
		}
		if (c == 'Ö') {
			return new char[]{'O', 'e'};
		}
		if (c == 'ü') {
			return new char[]{'u', 'e'};
		}
		if (c == 'Ü') {
			return new char[]{'U', 'e'};
		}
		if (c == 'ß') {
			return new char[]{'s', 's'};
		}
		return new char[]{c};
	}

	private static boolean isUmlaut(final char c) {
		char cl = Character.toLowerCase(c);
		return cl == 'ä' || cl == 'ö' || cl == 'ü' || cl == 'ß';
	}

	public static String wordsToAlphabet(String text) {
		Set<String> words = new HashSet<String>();
		String[] ws = text.split(" ");
		for (String w : ws) {
			words.add(w);
		}

		Map<String, Character> wordsToCharacter = new HashMap<String, Character>();
		String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		int pos = 0;
		for (String word : words) {
			if (!wordsToCharacter.containsKey(word)) {
				wordsToCharacter.put(word, alphabet.charAt(pos++));
			}
		}

		StringBuffer result = new StringBuffer();
		for (String w : ws) {
			result.append(wordsToCharacter.get(w));
		}
		return result.toString();
	}

	public static int scoreDE(final String text) {
		return score(text, bigramFrequencyDE);
	}
	public static int scoreEN(final String text) {
		return score(text, bigramFrequencyEN);
	}

	public static int calculateFitness(String pt, Language language) {
		if (Language.EN == language) {
			return scoreEN(pt);
		} else if (Language.DE == language) {
			return scoreDE(pt);
		} else {
			return 0;
		}

	}

	public static int score(final String text, int[][] bigramFrequency) {
		if (text == null || text.isEmpty()) {
			return 0;
		}

        String stripped = getStripped(text).toUpperCase();

		int score = 0;
		for (int i = 0; i < stripped.length() - 1; i++) {
			char c = stripped.charAt(i);
			char c1 = stripped.charAt(i + 1);
			//if (Character.isUnicodeIdentifierPart(c) && Character.isUpperCase(c1)) {
				int sco = bigramFrequency[c - 'A'][c1 - 'A'];
				score += sco;
			//}
		}

		return score;
	}

    static public int getFromChar(char c) {
        return c-'A'+1;
    }


    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    public boolean isText(String pt, int i) {
        // TODO
        return false;  //To change body of created methods use File | Settings | File Templates.
    }

    public boolean isWordText(String pt, int i) {
        //TODO: implement method
        return false;  //To change body of created methods use File | Settings | File Templates.
    }


    /*
        substitute Umlaute
        only letters
     */
    public static String getStripped(String text) {
        StringBuffer stripped = new StringBuffer();
        for (char c : text.toCharArray()) {
            if (isUmlaut(c)) {
                stripped.append(getUmlautChars(c));
            } else  if (Character.isLetter(c)) {
                stripped.append(c);
            }
        }
        return stripped.toString();
    }

	public static String getStripped(String text, String alphabet) {
		StringBuffer stripped = new StringBuffer();
		for (char c : text.toCharArray()) {
			if (isUmlaut(c)) {
				stripped.append(getStripped(new String(getUmlautChars(c))));
			} else  if (alphabet.indexOf(c) >= 0) {
				stripped.append(c);
			}
		}
		return stripped.toString();
	}

    /**
     * @param args
     */
    public static void main(String[] args) {
        String ct = "141518040015192319150023191514191923140023191519230023191514191923140015191900151920001919001519192314140015192319150015191923141400231915192314";
        Text text = new Text();


    }

	public static String splitWords(String text, int n) {
		StringBuilder result = new StringBuilder();
		String[] lines = text.split("\n");
		for (String line : lines) {
			for (int i = 0; i < line.length(); i++) {
				result.append(line.charAt(i));
				if ((i + 1) % n == 0) {
					result.append(" ");
				}
			}
			result.append("\n");
		}
		return result.toString();
	}

	public static String replaceEquals(String text, String equals) {
		StringBuilder result = new StringBuilder(text.length());
		for (char c : text.toCharArray()) {
			result.append(getCharReplaceEquals(c, equals));
		}

		return result.toString();
	}

	private static char getCharReplaceEquals(char c, String equalChars) {
		for (int i = 0; i < equalChars.length(); i+=2) {
			if (equalChars.charAt(i) == c) {
				return equalChars.charAt(i + 1);
			}
		}
		return c;
	}

	public static String getAlphabet(String alphabet, String key, boolean reverse) {
		String k = eliminateDoubles(key);
		if (k == null) {
			k = "";
		}
		StringBuilder restAlphabet = new StringBuilder(alphabet);
		for (char c : alphabet.toCharArray()) {
			if (k.indexOf(c) >= 0) {
				restAlphabet.deleteCharAt(restAlphabet.indexOf(String.valueOf(c)));
			}
		}
		if (reverse) {
			return k + restAlphabet.reverse();
		} else {
			return k + restAlphabet;
		}
	}

	private static String eliminateDoubles(String key) {
		if (key == null) {
			return null;
		}
		StringBuilder k = new StringBuilder();
		StringBuilder chars = new StringBuilder();

		for (char c : key.toCharArray()) {
			if (chars.indexOf(String.valueOf(c)) < 0) {
				chars.append(c);
				k.append(c);
			}
		}

		return k.toString();
	}

	public static List<String> getWords(String text) {
		List<String> words = new ArrayList<String>();
		String[] lines = text.split("\n");
		for (String line : lines) {
			words.addAll(Arrays.asList(line.split(" ")));
		}
		return words;
	}

	public static boolean isEmpty(String s) {
		return s == null || s.isEmpty();
	}
}