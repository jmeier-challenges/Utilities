package jm.tools;

import java.util.ArrayList;
import java.util.List;

public class Primfaktoren {

	private static void teilen(int zahl, List<Integer> faktoren) {
		int i;
		int max;
		max = (int) Math.round(Math.sqrt((double) zahl));

		if (zahl <= 1) {
			return;
		}

		if (zahl % 2 == 0) {
			faktoren.add(2);
			teilen(zahl / 2, faktoren);
			return;
		}

		for (i = 3; i <= max; i = i + 2) {

			if (zahl % i == 0) {
				teilen(i, faktoren);
				teilen(zahl / i, faktoren);
				return;
			}
		}
		faktoren.add(zahl);
	}
	
	public static List<Integer> getPrimes(int i) {
		List<Integer> faktoren = new ArrayList<Integer>();
		teilen(i, faktoren);
		return faktoren;
	}

	// *******************************************************************************
	public static void main(String[] args) {
		for (int zahl = 1; zahl < 1000; zahl++) {
			System.out.print(zahl + " = ");
			List<Integer> faktoren = new ArrayList<Integer>();
			teilen(zahl, faktoren);
			System.out.println(faktoren);
		}
	}
}
