package jm.tools;

import java.io.IOException;
import java.io.InputStreamReader;

import java.io.BufferedReader;
import java.net.*;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * User: johann
 * Date: 28.05.11
 * Time: 12:13
 */
public class UrlTool {
    public static void main(String[] args) throws Exception {
        UrlTool tool = new UrlTool();
        tool.show("http://inselfieber.happy-security.de");
        //tool.show("http://localhost");
    }

    private void show(String urlString) throws Exception {
        //URL url = new URL(urlString+"?site=8dd5d");
        //connection.setRequestMethod("GET");

        //connection.setRequestProperty("User-Agent", "hello");
        //connection.setRequestProperty("Cookie", "name=johann");

        //printRequestHeaders(connection);
        //System.out.println();

        //printResponseHeaders(connection);
        //System.out.println();

        for (int i = 0x10000; i < 0x100000; i++) {
            String site = Integer.toHexString(i);
            URL url = new URL(urlString+"?site=" + site);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            if (isValidSite(connection)) {
                System.out.println("Valid site: " + site);
            }
            if (i%0x500==0) {
                System.out.println(site);
            }
        }

    }

    private void printResponseContent(URLConnection connection) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
        }
        reader.close();
    }

    private boolean isValidSite(URLConnection connection) throws IOException {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                if (!line.contains("Wir freuen uns")) {
                    return false;
                }
            }
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return true;
    }

    private void printResponseHeaders(HttpURLConnection connection) {
        Map<String, List<String>> headers = connection.getHeaderFields();
        Set<String> keys = headers.keySet();
        for (String key : keys) {
            System.out.println((key==null?"":key + ": ") + headers.get(key));
            //System.out.println(key + ": " + headers.get(key));
        }
    }

    private void printRequestHeaders(HttpURLConnection connection) {
        System.out.println("Method: " + connection.getRequestMethod());

        Map<String, List<String>> headers = connection.getRequestProperties();
        Set<String> keys = headers.keySet();
        for (String key : keys) {
            System.out.println(key==null?"":key + ": " +headers.get(key));
        }
    }

    /*
    private void useSocket(String url) {
        Socket socket = new Socket("localhos", Socket.);
    }
    */
}
