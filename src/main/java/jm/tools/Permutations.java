package jm.tools;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Permutations {
	private PermutationVisitor visitor = null;

    private void perm3(String s, int count) {
    	perm3("", s, count);
    }

    private void perm3(String prefix, String s, int count) {
        int N = s.length();
        if (count == 0) {
        	visitor.visitPermutation(prefix);
        }
        else {
            for (int i = 0; i < N; i++) {
               perm3(prefix + s.charAt(i),
            		   s.substring(0, i) + s.substring(i+1, N),
            		   count-1);
            }
        }
    }

    public void permutate(String alphabet, PermutationVisitor visitor ) {
    	this.visitor = visitor;
    	perm3(alphabet, alphabet.length());
    }

    public void permutate(String alphabet, PermutationVisitor visitor, int count) {
    	this.visitor = visitor;
    	perm3(alphabet, count);
    }

    public void permutate(List<Object> alphabet, PermutationVisitor visitor) {
    	this.visitor = visitor;
    	permObjekt(alphabet, alphabet.size());
    }

    public void permutate(List<Object> alphabet, PermutationVisitor visitor, int length) {
    	this.visitor = visitor;
    	permObjekt(alphabet, length);
    }

    private void permObjekt(List<Object> alphabet, int count) {
    	permObjekt(new ArrayList<Object>(), alphabet, count);
    }

    private void permObjekt(List<Object> prefix, List<Object> alphabet, int count) {
        int N = alphabet.size();
        if (count == 0) {
        	visitor.visitPermutation(prefix);
        }
        else {
            for (int i = 0; i < N; i++) {
            	Object o = alphabet.remove(i);
            	prefix.add(o);
            	permObjekt(prefix,
            		   alphabet,
            		   count-1);
            	prefix.remove(prefix.size()-1);
            	alphabet.add(i, o);
            }
        }
    }

    public static void main(String[] args) throws IOException {
//    	permTest1();
    	permTest2();
    }

	private static void permTest2() {
		Permutations permut = new Permutations();
    	List<Object> alphabet = new ArrayList<Object>();
    	alphabet.add("DE");
    	alphabet.add("D1");
    	alphabet.add("FA");
    	alphabet.add("CE");
    	permut.permutate(alphabet, new PermutationVisitor() {

    		public void visitPermutation(String permutation) {
				System.out.println(permutation);
			}

			public void visitPermutation(List<Object> permutation) {
				System.out.println(permutation);
			}
		}, 4);
	}
	private static void permTest1() {
		Permutations permut = new Permutations();
    	String alphabet = "1234";
    	permut.permutate(alphabet, new PermutationVisitor() {

    		public void visitPermutation(String permutation) {
				System.out.println(permutation);
			}

			public void visitPermutation(List<Object> prefix) {
				// TODO Auto-generated method stub

			}
		}, 2);
	}

}
