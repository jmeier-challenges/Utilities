package jm.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/*
 * Password is CRASHING
 */
public class PasswordFinder {
	
	static String [] passwordFiles = {"../wortlisten/length08.txt", "../wortlisten/words_german.txt"};
	static File passwordFile = null;
	static File fileToCheck = null;
	static ArrayList<String> passwords = new ArrayList<String>();
	static ArrayList<String> wordsToCheck = new ArrayList<String>();

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		fileToCheck = new File("heavy1.txt");
		for (String filename : passwordFiles) {
			passwordFile = new File(filename);
			check(fileToCheck, passwordFile);
		}

	}

	private static void check(File fileToCheck2, File passwordFile2) throws IOException {
		int pos = 0;
		readPasswords(passwordFile2);
		System.out.println("Processing file " + fileToCheck2);
		BufferedReader reader = new BufferedReader(new FileReader(fileToCheck2));
		String line = reader.readLine();
		while (line != null) {
			if (++pos % 1000 == 0) {
				System.out.println("Pos: " + pos + " " + line);
			}
			if (passwords.contains(line.toUpperCase())) {
				System.out.println("Found: " + line);
			}
			line = reader.readLine();
		};
	}

	private static void readPasswords(File passwordFile2) throws IOException {
		System.out.println("Loading password file " + passwordFile2);
		BufferedReader reader = new BufferedReader(new FileReader(passwordFile2));
		String line = null;
		do {
			line = reader.readLine();
			if (line != null) {
				passwords.add(line.toUpperCase());
			}
		} while (line != null);
	}

}
