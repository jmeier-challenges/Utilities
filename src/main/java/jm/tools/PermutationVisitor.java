package jm.tools;

import java.util.List;

public interface PermutationVisitor {
	void visitPermutation(String permutation);

	void visitPermutation(List<Object> prefix);
}

