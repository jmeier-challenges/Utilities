package jm.tools;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Johann
 * Date: 11.01.11
 * Time: 19:28
 */
public class Wordlist {
    private List<String> words = new ArrayList<String>();
    private int minLength = 0;
    private int maxLength = 0;
    private boolean onlyLowercase = false;

    public void addWordlist(String filename) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        String line = null;
        while ((line = reader.readLine()) != null) {
            if (shouldAdd(line)) {
                if (onlyLowercase) {
                    words.add(line.toLowerCase());
                } else {
                    words.add(line.toLowerCase());
                }
            }
        }
        System.out.println(words);
    }

    private boolean shouldAdd(String line) {
        int length = line.length();
        if (minLength > 0 && length < minLength) {
            return false;
        }

        if (maxLength > 0 && length > maxLength) {
            return false;
        }

        return true;
    }


    public boolean contains(String word) throws Exception {
        return words.contains(word);
    }

    public static void main(String[] args) throws Exception {
        Wordlist wordlist = new Wordlist();
        wordlist.setMaxLength(6);
        wordlist.setMinLength(6);
        wordlist.setLowercase();
        wordlist.addWordlist("wordlists/words.german.txt");
        System.out.println(wordlist.contains("ajejim"));
        System.out.println(wordlist.contains("Abakus"));
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public void setLowercase() {
        onlyLowercase = true;
    }
}
