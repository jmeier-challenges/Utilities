package jm.tools;

/**
 * User: T05365A
 * Date: 11.10.12
 * Time: 09:46
 */
public class Permutation {
	/*
		http://www.cut-the-knot.org/do_you_know/AllPerm.shtml
		 */
	public static void nextPermutation(int[] permutation) {
		if (!hasNextPermutation(permutation)) {
			return;
		}

		int N = permutation.length;

		int i = N - 1;
		while (permutation[i - 1] >= permutation[i]) {
			i = i - 1;
		}

		int j = N;
		while (permutation[j - 1] <= permutation[i - 1]) {
			j = j - 1;
		}

		swap(permutation, i - 1, j - 1); // swap values at positions (i-1) and (j-1)

		i++; j = N;
		while (i < j)
		{
			swap(permutation, i-1, j-1);
			i++;
			j--;
		}
	}

	public static boolean hasNextPermutation(int[] permutation) {
		int N = permutation.length;

		int i = N - 1;
		while (permutation[i - 1] >= permutation[i]) {
			i = i - 1;
			if (i == 0) {
				return false;
			}
		}
		return true;
	}

	private static void swap(int[] permutation, int i, int j) {
		int temp = permutation[i];
		permutation[i] = permutation[j];
		permutation[j] = temp;
	}
}
