package jm.tools;

/* Miller-Rabin Primzahltest */

public class PrimzahlTest {

	private static long[] as = { 2, 7, 61 };

	private static long modpow(long x, long c, long m) {
		long result = 1;
		long aktpot = x;
		while (c > 0) {
			if (c % 2 == 1) {
				result = (result * aktpot) % m;
			}
			aktpot = (aktpot * aktpot) % m;
			c /= 2;
		}
		return result;
	}

	private static boolean millerRabin(long n) {
		outer: for (long a : as) {
			if (a < n) {
				long s = 0;
				long d = n - 1;
				while (d % 2 == 0) {
					s++;
					d /= 2;
				}
				long x = modpow(a, d, n);
				if (x != 1 && x != n - 1) {
					for (long r = 1; r < s; r++) {
						x = (x * x) % n;
						if (x == 1) {
							return false;
						}
						if (x == n - 1) {
							continue outer;
						}
					}
					return false;
				}
			}
		}
		return true;
	}

	public static boolean isPrime(int n) {
		if (n <= 1) {
			return false;
		} else if (n <= 3) {
			return true;
		} else if (n % 2 == 0) {
			return false;
		} else {
			return millerRabin(n);
		}
	}
}
