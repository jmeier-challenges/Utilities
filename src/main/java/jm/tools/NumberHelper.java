package jm.tools;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class NumberHelper {
	public static int digitSum(int n) {
		String number = Integer.toString(n);
		int sum = 0;
		for (int i = 0; i < number.length(); i++) {
			sum += Integer.parseInt(number.substring(i, i + 1));
		}
		return sum;
	}
	
	public static List<Integer> getDivisors(int zahl) {
		List<Integer> divisors = new ArrayList<Integer>();
		for (int i = zahl/2; i > 1; i--) {
			if (zahl % i == 0) {
				divisors.add(i);
			}
		}
		return divisors;
	}

	public static void main(String[] args) {
		System.out.println(getDivisors(243));
	}

    public static int digitSum(BigInteger n) {
        String number = n.toString();
        int sum = 0;
        for (int i = 0; i < number.length(); i++) {
            sum += Integer.parseInt(number.substring(i, i + 1));
        }
        return sum;
    }
}
