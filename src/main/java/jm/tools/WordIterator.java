package jm.tools;

/**
 * User: T05365A
 * Date: 06.11.12
 * Time: 10:13
 */
public class WordIterator {
	private String text;
	private int pos = 0;
	private String alphabet;
	private String delimiters = " ";


	public boolean hasNext() {
		return pos < text.length();
	}

	public String next() {
		String cs = text.substring(pos, pos + 1);
		return nextWord(alphabet.contains(cs));
	}

	private String nextWord(boolean alphabetWord) {
		StringBuilder word = new StringBuilder();
		while (pos < text.length()) {
			String cs = text.substring(pos, pos + 1);
			if (delimiters.contains(cs)) {
				pos++;
				break;
			}
			if (alphabet.contains(cs) ^ !alphabetWord) {
				word.append(cs);
			} else {
				break;
			}
			pos++;
		}
		return word.toString();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		pos = 0;
		this.text = text;
	}

	public String getAlphabet() {
		return alphabet;
	}

	public void setAlphabet(String alphabet) {
		this.alphabet = alphabet;
	}

	public static void main(String[] args) {
		WordIterator iterator = new WordIterator();
		iterator.setAlphabet("0123456789");
		iterator.setText("012 kj kj .451 k 66 jk");
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
}
