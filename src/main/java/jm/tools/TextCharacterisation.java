package jm.tools;

/**
 * User: johann
 * Date: 28.07.12
 * Time: 13:39
 */
public class TextCharacterisation {
    public static double[] germanFrequency = {6.51, 1.89, 3.06, 5.08, 17.4, 1.66, 3.01, 4.76, 7.55, 0.27, 1.21, 3.44, 2.53, 9.78, 2.51, 0.79, 0.02, 7.00, 7.27, 6.15, 4.35, 0.67, 1.89, 0.03, 0.04, 1.13};
    public static double[] englishFrequency = {8.167, 1.492, 2.782, 4.253, 12.702, 2.228, 2.015, 6.094, 6.966, 0.153, 0.772, 4.025, 2.406, 6.749, 7.507, 1.929, 0.095, 5.987, 6.327, 9.056, 2.758, 0.978, 2.360, 0.150, 1.974, 0.074};
    public static double[] frenchFrequency = {7.636, 0.901, 3.260, 3.669, 14.715, 1.066, 0.866, 0.737, 7.529, 0.545, 0.049, 5.456, 2.968, 7.095, 5.378, 3.021, 1.362, 6.553, 7.948, 7.244, 6.311, 1.628, 0.114, 0.387, 0.308, 0.136};

    /*
    http://practicalcryptography.com/cryptanalysis/text-characterisation/chi-squared-statistic/
     */
    public static double chiSquare(double[] frequency, String text) {
        double chi = 0;

        int[] textFrequency = getFrequency(text);
        int length = 0;
        for (int f : textFrequency) {
            length += f;
        }

        for (int i = 0; i < textFrequency.length; i++) {
            int c = textFrequency[i];
            double e = frequency[i] * length / 100;
            chi = chi + (c - e)*(c - e) / e;
        }

        return chi;
    }

    public static int[] getFrequency(String text) {
        int[] frequency = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        for (char c : text.toUpperCase().toCharArray()) {
            if (c >= 'A' && c <= 'Z') {
                frequency[c - 'A'] += 1;
            }
        }

        return frequency;
    }

    public static double chiSquareDe(String text) {
        return chiSquare(germanFrequency, text);
    }

    public static double chiSquareEn(String text) {
        return chiSquare(englishFrequency, text);
    }

    public static double chiSquareFr(String text) {
        return chiSquare(frenchFrequency, text);
    }

    public static void main(String[] args) {
        double[] frequency = frenchFrequency;
        double ic = 0;

        for (double f : frequency) {
            ic += (f / 100) * (f / 100);
        }
        System.out.println(ic);
    }

}

