package jm.tools.cipher.substitution.view;
import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;

/**
 * User: T05365A
 * Date: 22.12.10
 * Time: 15:27
 */
public class SubstitutionMain {

    public static void main(String[] argc) {

        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
    private static void createAndShowGUI() {
        SubstitutionForm form = new SubstitutionForm();
        //Create and set up the window.
        JFrame frame = new JFrame("HelloWorldSwing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add the ubiquitous "Hello World" label.
        //javax.swing.JLabel label = new JLabel("Hello World");

        frame.getContentPane().add(form.getMainPanel());

        JPanel cipherCharPanel = form.cipherCharPanel;

        //Display the window.
        frame.pack();
        frame.setVisible(true);
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        //addChars(cipherCharPanel, alphabet);
        //addChars(form.clearCharPanel, "MMMMMMMMMMMMMMMMMMMMMMMMM");
        fillCharGrid(form.charGrid);
    }

    private static void fillCharGrid(JTable table) {
        table.setModel(new TableModel() {
            //private List<Character> cChars = new ArrayList<Character>();
            //private List<Character> pChars = new ArrayList<Character>();
            private String cChars = "abcdefghijklmnopqrstuvwxyz";
            private String pChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            public int getRowCount() {
                //return cChars.size();
                return cChars.length();
            }

            public int getColumnCount() {
                return 2;
            }

            public String getColumnName(int i) {
                if (i == 0) {
                    return "CC";
                } else {
                    return "PC";
                }
            }

            public Class<?> getColumnClass(int i) {
                return Character.class;
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public Object getValueAt(int row, int col) {
                if (col == 0) {
                    //return cChars.get(row);
                    return cChars.substring(row, row+1);
                } else {
                    //return pChars.get(row);
                    return pChars.substring(row, row+1);
                }
            }

            public void setValueAt(Object o, int i, int i1) {
                // do nothing
            }

            public void addTableModelListener(TableModelListener tableModelListener) {
            }

            public void removeTableModelListener(TableModelListener tableModelListener) {
            }

        });

    }

    private static void addChars(JPanel panel, String chars) {

        for (int i = 0; i < chars.length(); i++) {
            String c = chars.substring(i, i + 1);
            JLabel charLabel = new JLabel();
            charLabel.setText(c);
            charLabel.setMinimumSize(new Dimension(60, -1));
            panel.add(charLabel);
        }
    }
}
