package jm.tools.cipher.substitution.view;

import javax.swing.*;

/**
 * User: T05365A
 * Date: 22.12.10
 * Time: 15:18
 */
public class SubstitutionForm {
    private javax.swing.JPanel mainPanel;
    private JPanel statisticPanel;
    private JPanel alphabetPanel;
    private JPanel ctPanel;
    private JPanel substitutionPanel;
    private JPanel ptPanel;
    private JTextField alphabetTextField;
    private JLabel alphabetLabel;
    private javax.swing.JTextArea ctTextArea;
    private JLabel ctLabel;
    private JTextArea ptTextArea;
    private JLabel ptLabel;
    private JTextField textField1;
    private javax.swing.JLabel messageLabel;
    JPanel cipherCharPanel;
    private JPanel testPanel;
    public JPanel clearCharPanel;
    JTable charGrid;

    public SubstitutionForm() {
        ctTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
            public void focusGained(java.awt.event.FocusEvent e) {
                messageLabel.setText("gained");
                super.focusGained(e);    //To change body of overridden methods use File | Settings | File Templates.
            }

            public void focusLost(java.awt.event.FocusEvent e) {
                messageLabel.setText("lost");
                super.focusLost(e);    //To change body of overridden methods use File | Settings | File Templates.
            }
        });
    }

    public javax.swing.JPanel getMainPanel() {
        return testPanel;
    }
}
