package jm.tools.cipher.substitution.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class Substitution {

	final static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";
	final static String LARGE_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	final static String ALPHABET_DE = "abcdefghijklmnopqrstuvwxyzäöüß";

	Map<String, String> subst = new HashMap<String, String>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Substitution s = new Substitution();
		s.initializeLarge();
		String cipherText;
//		cipherText = "YQWJSJU, IYLO OYQFGB EJ XQKOLBJUJB VO KQIYLKM AQUJ IYVK VK JKIJUIVLKLKM MVAJ. OFEOILIFILQK XLZYJUO, VKB XVJOVU XLZYJUO LK ZVUILXFGVU, QNNJU VGAQOI KQ OJXFULIT VI VGG. IYJT VUJ JHIUJAJGT JVOT IQ EUJVC VKB VUJ EJOI SLJWJB QKGT VO NFK ZFPPGJO.";
		cipherText = "abcdefghifgjbcklmnojkmbcpmqfkmoferpesmthgmubhmdbgmjglmvpeumqpiimrbcpmwpiibqmofyApelmnopmxebtipzmqjgomgojkmyjxopemjkmgofgmgopmApumjkmxepggumibcdlmBmqjiimybzpmhxmqjgomfmtpggpempcyeuxgjbcmkopzpmfcumkbbclmCbhemkbihgjbcmjkDmpwiicpoteidel";
		printFrequency(s.getLetterFrequency(cipherText));
		System.out.println(cipherText);
		System.out.println(s.substitute(cipherText));
	}

	private static void printFrequency(Map<String, Integer> frequencies) {
		System.out.println(sortByValue(frequencies));
	}

	public void initialize() {
		subst.put("a", "a"); subst.put("n", "n");
		subst.put("b", "o"); subst.put("o", "o");
		subst.put("c", "c"); subst.put("p", "t");
		subst.put("d", "d"); subst.put("q", "o");
		subst.put("e", "e"); subst.put("r", "r");
		subst.put("f", "f"); subst.put("s", "v");
		subst.put("g", "a"); subst.put("t", "t");
		subst.put("h", "h"); subst.put("u", "r");
		subst.put("i", "i"); subst.put("v", "v");
		subst.put("j", "n"); subst.put("w", "w");
		subst.put("k", "k"); subst.put("x", "x");
		subst.put("l", "l"); subst.put("y", "h");
		subst.put("m", "e"); subst.put("z", "z");

		subst.put(",", ","); subst.put(".", ".");
		subst.put(" ", " "); subst.put(":", ":");
	}

    public void initializeLarge() {
        subst.put("a", "S"); subst.put("n", "n");
        subst.put("b", "a"); subst.put("o", "k");
        subst.put("c", "m"); subst.put("p", "y");
        subst.put("d", "u"); subst.put("q", "o");
        subst.put("e", "e"); subst.put("r", "f");
        subst.put("f", "l"); subst.put("s", "r");
        subst.put("g", "*"); subst.put("t", "g");
        subst.put("h", "i"); subst.put("u", "w");
        subst.put("i", "t"); subst.put("v", "c");
        subst.put("j", "h"); subst.put("w", "s");
        subst.put("k", "*"); subst.put("x", "d");
        subst.put("l", "*"); subst.put("y", "v");
        subst.put("m", "T"); subst.put("z", "x");
        subst.put("A", "."); subst.put("N", "I");
        subst.put("B", "Y"); subst.put("O", "*");
        subst.put("C", "p"); subst.put("P", "*");
        subst.put("D", "*"); subst.put("Q", "*");
        subst.put("E", "*"); subst.put("R", "*");
        subst.put("F", "*"); subst.put("S", "*");
        subst.put("G", "*"); subst.put("T", "*");
        subst.put("H", "b"); subst.put("U", "*");
        subst.put("I", "*"); subst.put("V", "*");
        subst.put("J", "*"); subst.put("W", "*");
        subst.put("K", "C"); subst.put("X", "*");
        subst.put("L", "*"); subst.put("Y", "*");
        subst.put("M", "*"); subst.put("Z", "*");

        subst.put(",", "*"); subst.put(".", "*");
        subst.put(" ", " "); subst.put(":", "*");
        subst.put("!", "*");
    }

	public String substitute(String cipherText) {
		StringBuffer text = new StringBuffer();
		for (int i = 0; i < cipherText.length(); i++) {
			text.append(subst.get(cipherText.substring(i, i+1)));
		}
		return text.toString();
	}

	public Map<String, Integer> getLetterFrequency(String text) {
		Map<String, Integer> frequency = new HashMap<String, Integer>();
		for (int i = 0; i < text.length(); i++) {
			String character = text.substring(i, i+1);
			Integer count = frequency.get(character);
			if (count == null) {
				count = 1;
			} else {
				count = count + 1;
			}

			frequency.put(character, count);
		}

		return frequency;
	}


	static Map<String, Integer> sortByValue(Map<String, Integer> map) {
		LinkedList<Entry<String, Integer>> list = new LinkedList<Entry<String,Integer>>(map.entrySet());
		Collections.sort(list, new Comparator<Entry<String, Integer>>() {
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {
				return o2.getValue().compareTo((o1).getValue());
			}
		});
		Map<String, Integer> result = new LinkedHashMap<String, Integer>();
		for (Iterator<Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
			Map.Entry<String, Integer> entry = it.next();
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}
