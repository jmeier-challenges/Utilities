package jm.tools;

import java.util.*;

/**
 * User: johann
 * Date: 04.01.12
 * Time: 12:24
 */
public class CharacterFrequency {
    private static final Comparator<Entry> countComparator = new Comparator<Entry>() {
        public int compare(Entry e1, Entry e2) {
            return e2.getValue().compareTo(e1.getValue());
        }
    };

    private Comparator<Entry> comparator = null;

    class Entry implements Comparable<Entry> {
        private Character key;
        private Integer value;

        Entry(Character key, Integer value) {
            this.key = key;
            this.value = value;
        }

        public Character getKey() {
            return key;
        }

        public Integer getValue() {
            return value;
        }

        public void increment() {
            this.value++;
        }

        public int compareTo(Entry entry) {
            if (comparator == null) {
                return key.compareTo(entry.key);
            } else {
                return comparator.compare(this, entry);
            }
        }
        
        public String toString() {
            return "'" +key + "' = " + value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry entry = (Entry) o;

            if (key != null ? !key.equals(entry.key) : entry.key != null) return false;
            if (value != null ? !value.equals(entry.value) : entry.value != null) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = key != null ? key.hashCode() : 0;
            result = 31 * result + (value != null ? value.hashCode() : 0);
            return result;
        }
    }
    
    private List<Entry> charCounts = new ArrayList<Entry>();

    /*
   Deutsch : enisrat
   Englisch: etaoins
    */
    public List<Entry> countChars(String text) {
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            Entry entry = getEntry(c);
            entry.increment();
        }

        return getSortedByCount();
    }

    /**
     * creates new Entry and add it to list, if not found!
     * @param c Character to find
     * @return Entry for Character c
     */
    Entry getEntry(char c) {
        for (Entry entry : charCounts) {
            if (entry.getKey().equals(c)) {
                return entry;
            }
        }
        Entry newEntry = new Entry(c, 0);
        charCounts.add(newEntry);
        return newEntry;
    }

    public List<Entry> getSortedByCount() {
        comparator = countComparator;
        Collections.sort(charCounts);
        return charCounts;
    }

    public List<Entry> getSortedByKey() {
        comparator = null;
        Collections.sort(charCounts);
        return charCounts;
    }

    public static void main(String[] args) {
        CharacterFrequency freq = new CharacterFrequency();
        String text = "Dieser Test ist rein fuer das ausprobieren der neuen Klasse gedacht.";
        List<Entry> f = freq.countChars(text.toLowerCase());
        System.out.println(f);
    }

    
}
