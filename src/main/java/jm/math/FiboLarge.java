package jm.math;

import numbercruncher.mathutils.BigFunctions;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * User: johann
 * Date: 27.12.12
 * Time: 13:01
 *  fib(2*n) = fib(n) * (fib(n) + 2 * fib(n-1))
    fib(2*n-1) = fib(n)^2 + fib(n-1)^2
 */
public class FiboLarge {
    BigInteger Fprev;
    BigInteger Fcurr;

    BigInteger fibonacciSequence(long n) {
        BigInteger curr,prev;

        if (n == 1) {
            Fprev = BigInteger.ZERO;
            Fcurr = BigInteger.ONE;
        } else {
            fibonacciSequence(n / 2);
            prev = Fcurr.multiply(Fcurr).add(Fprev.multiply(Fprev));
            curr = Fcurr.multiply(Fcurr.add(Fprev.multiply(BigInteger.valueOf(2))));

            if (n % 2 == 1)//if n is odd
            {
                Fprev = curr;
                Fcurr = curr.add(prev);
            } else {
                Fprev = prev;
                Fcurr = curr;
            }
        }
        return Fcurr;
    }

    BigInteger fibonacci(long n){
        return fibonacciSequence(n);
    }

    public static void main(String[] args) {
        long n=150000000;
        FiboLarge fibo = new FiboLarge();
        BigInteger result = fibo.fibonacci(n);
        System.out.println(BigFunctions.ln(new BigDecimal(result), 5));
    }


}
