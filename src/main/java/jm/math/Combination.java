package jm.math;


/**
 * User: T05365A
 * Date: 13.04.11
 * Time: 10:27
 */
interface CombiListener {
	void permutation(String perm);
}

public class Combination {
	private int[] bits;

	private int sum;
	private CombiListener listener;

	private void permutateZerosAndOnes(int zeros, int ones) {
		sum = 0;
		int n = zeros + ones;
		bits = new int[n];
		for (int i = 0; i < bits.length; i++) {
			bits[i] = -1;
		}
		int bitPos = 0;
		for (int i = 0; i <= zeros; i++) {
			bits[bitPos] = i;
			permutateZerosAndOnesRek(i + 1, n, ones - 1, bitPos + 1);
			bits[bitPos] = -1;
		}
		System.out.println(sum);
	}
	private void permutateZerosAndOnesRek(int begin, int end, int count, int bitPos) {
		if (count == 0) {
			print();
			return;
		}
		for (int i = begin; i < end; i++) {
			bits[bitPos] = i;
			permutateZerosAndOnesRek(i + 1, end, count - 1, bitPos + 1);
			bits[bitPos] = -1;
		}
	}

	private void print() {
		StringBuffer binString = new StringBuffer();
		for (int a = 0; a < bits.length; a++) {
			boolean isOne = false;
			for (Integer integer : bits) {
				if (a == integer) {
					isOne = true;
				}
			}
			if (isOne) {
				binString.append("1");
			} else {
				binString.append("0");
			}
		}
		//System.out.println(binString);
		if (listener != null) {
			listener.permutation(binString.toString());
		};
		sum++;
	}

	public static void main(String[] args) {
		Combination combi = new Combination();
		combi.setListener(new CombiListener() {

			public void permutation(final String perm) {
				//System.out.println(perm);
			}
		});
		combi.permutateZerosAndOnes(90, 100);
		System.out.println();
	}

	private void setListener(final CombiListener combiListener) {
		listener = combiListener;
	}
}