package jm.math;

/**
 * User: T05365A
 * Date: 12.10.12
 * Time: 08:18
 */
public class BaseArithmetik {
	private int base;

	public BaseArithmetik(int base) {
		this.base = base;
	}

	public void increase(int[] key) {
		for (int i = 0; i < key.length; i++) {
			int sum = key[i] + 1;
			int add = 0;
			if (sum == base) {
				add = 1;
				key[i] = 0;
			} else {
				key[i] = sum;
			}
			if (add == 0) {
				break;
			}
		}
	}
}
