package jm.math;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: johann
 * Date: 31.12.10
 * Time: 10:57
 * To change this template use File | Settings | File Templates.
 */
public class Primenumber {
    boolean[] primes = null;

    public Primenumber() {
        int n = 1000;
        primes = new boolean[n+1];
        try {
            sieve(n);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Primenumber(int n) {
        primes = new boolean[n+1];
        try {
            sieve(n);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPrime(int i) throws Exception {
        if (i < primes.length) {
            return primes[i];
        } else {
            throw new Exception("Only primes up to " + primes.length);
        }
    }

    public int getNthPrime(int n) throws Exception {
        int count = 0;
        int prime = 0;

        for (int i = 0 ; i < primes.length; i++) {
            if (isPrime(i)) {
                count++;
            }
            if (count == n) {
                prime = i;
                break;
            }
        }
        return prime;
    }

    public List<Integer> getPrimes() throws Exception {
        List<Integer> prims = new ArrayList<Integer>();
        for (int i = 0; i < primes.length; i++) {
            if (isPrime(i)) {
                prims.add(Integer.valueOf(i));
            }
        }
        return prims;
    }

    public List<Integer> getFirstPrimes(int count) throws Exception {
        List<Integer> prims = new ArrayList<Integer>();
        for (int i = 0; i < primes.length; i++) {
            if (isPrime(i)) {
                prims.add(Integer.valueOf(i));
            }
        }
        if (prims.size() < count) {
           throw new RuntimeException("To few primes " + prims.size() + " count " + count);
        } else if (prims.size() > count) {
            return prims.subList(0, count);

        } else {
            return prims;
        }
    }

    private void sieve(int n) throws Exception {
        init(n);
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (isPrime(i)) {
                removeMultiple(i);
            }
        }
    }

    private void removeMultiple(int n) {
        for (int i = 2*n; i < primes.length; i+=n) {
            primes[i] = false;
        }
    }

    private void init(int n) {
        for (int i = 2; i < n; i++) {
            primes[i] = true;
        }
        primes[0] = false;
        primes[1] = false;
    }
}
