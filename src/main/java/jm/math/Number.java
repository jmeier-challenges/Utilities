package jm.math;

import java.math.BigInteger;

/**
 * User: johann
 * Date: 24.12.10
 * Time: 15:06
 */
public class Number {
    public static final String DEC_DIGITS="0123456789";
    public static final String HEX_DIGITS="0123456789ABCDEF";

    /**
     * converts numbers with different bases and digits
     * @param argc
     *
     * 1. number
     * 2. from digits
     * 3. to digits
     */
    public static void main(String[] argc ) {
        if (argc.length != 3) {
            printUsage();
        } else {
            System.out.println(convert(argc[0], argc[1], argc[2]));
        }
    }

    private static void printUsage() {
        System.out.println("Usage: Number <number> <from digits> <to digits>");
    }

    /**
     * returns the hex value of the given number.
     * @param number
     * @return
     */
    public static String decToHex(BigInteger number) {
        return fromBigInteger(number, HEX_DIGITS);
    }

    /**
     * Converts a BigInteger to a String with the base from the length of toDigits
     * with the given digits.
     * @param number
     * @param toDigits
     * @return
     */
    public static String fromBigInteger(BigInteger number, String toDigits) {
        StringBuffer result = new StringBuffer();
        BigInteger base = BigInteger.valueOf(toDigits.length());
        BigInteger rest;
        BigInteger[] devideRemainder;
        do {
            devideRemainder = number.divideAndRemainder(base);
            rest = devideRemainder[1];
            number = devideRemainder[0];
            result.insert(0, toDigits.charAt(rest.intValue()));
        } while (number.compareTo(base) >= 0);
        if (number.intValue() > 0) {
            result.insert(0, toDigits.charAt(number.intValue()));
        }
        return result.toString();
    }

    /**
     * Converts the given "number"-String with the given "digits" to
     * the String-Value with the given to-"digits"
     * @param fromString
     * @param fromDigits
     * @param toDigits
     * @return calculated number string
     */
    public static String convert(String fromString, String fromDigits, String toDigits) {
        BigInteger decimalValue = toBigInteger(fromString, fromDigits);
        return fromBigInteger(decimalValue, toDigits);
    }

    /**
     *  Calculates the integer value from the given "number"-String
     *  with the given "digits".
     *
     * @param fromString
     * @param fromDigits
     * @return The calculated integer value
     */
    public static BigInteger toBigInteger(String fromString, String fromDigits) {
        BigInteger decimalValue = BigInteger.ZERO;
        BigInteger baseFrom = BigInteger.valueOf(fromDigits.length());
        int fromLength = fromString.length();

        for (int from = 0; from < fromLength; from++) {
            char digit = fromString.charAt(fromLength-1-from);
            BigInteger pos = BigInteger.valueOf(fromDigits.indexOf(digit));

            decimalValue = decimalValue.add(pos.multiply(baseFrom.pow(from)));
        }
        return decimalValue;
    }

    public static int digitSum(BigInteger number) {
        int sum = 0;
        String iString = number.toString();
        for (int i = 0; i < iString.length(); i++) {
            int digit = Integer.parseInt(iString.substring(i, i + 1));
            sum += digit;
        }
        return sum;
    }


}
