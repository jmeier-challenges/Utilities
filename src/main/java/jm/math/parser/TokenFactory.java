package jm.math.parser;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 13:59
 */
public class TokenFactory {
    public static Token createToken(String t) {
        Token token = null;
        if (OperatorToken.isOperator(t)) {
            token = new OperatorToken(t);
        } else {
            token = new IntegerToken(t);
        }
        return token;
    }
}
