package jm.math.parser;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 13:45
 */
public class Parser {

    public static BigInteger calculateInteger(String formula) {
        BigInteger result = BigInteger.ZERO;
        String[] parts = formula.split(" ");
        List<Object> tokens = new LinkedList<Object>();
        for (String part : parts) {
            Token token = TokenFactory.createToken(part);
            tokens.add(token);
        }

        return result;
    }


}
