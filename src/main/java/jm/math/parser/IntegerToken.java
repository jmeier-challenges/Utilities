package jm.math.parser;

import java.math.BigInteger;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 14:04
 */
public class IntegerToken implements Token {
    BigInteger value = null;
    public IntegerToken(String token) {
        value = new BigInteger(token);
    }
}
