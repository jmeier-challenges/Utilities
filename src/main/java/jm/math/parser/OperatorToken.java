package jm.math.parser;

/**
 * User: johann
 * Date: 28.12.10
 * Time: 13:51
 */
public class OperatorToken implements Token{
    enum Operator {Unknown, Mul, Div, Add, Sub};
    private Operator operator;
    private static final String operators="*/+-";

    public OperatorToken(String token) {
        parse(token);
    }

    private void parse(String token) {
        if ("*".equals(token)) {
            operator = Operator.Mul;
        }
        if ("/".equals(token)) {
            operator = Operator.Div;
        }
        if ("+".equals(token)) {
            operator = Operator.Add;
        }
        if ("-".equals(token)) {
            operator = Operator.Sub;
        }
    }

    public static boolean isOperator(String token) {
        return operators.contains(token);
    }
}
