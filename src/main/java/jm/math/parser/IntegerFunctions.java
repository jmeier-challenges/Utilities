package jm.math.parser;

import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: johann
 * Date: 30.12.10
 * Time: 19:47
 * To change this template use File | Settings | File Templates.
 */
public class IntegerFunctions {
    public static BigInteger fac(BigInteger n) {
        if (BigInteger.ZERO.equals(n)) {
            return BigInteger.ONE;
        }

        BigInteger prod = BigInteger.ONE;
        BigInteger i = BigInteger.ONE;
        while (i.compareTo(n) <= 0) {
            prod = prod.multiply(i);
            i = i.add(BigInteger.ONE);
        }
        return prod;
    }

    public static void main(String[] args) {
    }
}
