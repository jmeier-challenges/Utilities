package jm.math;

/**
 * User: T05365A
 * Date: 01.04.11
 * Time: 14:27
 * cycle 290 ?
 */
public class GameOfLive {
	private boolean[][] world;
	private boolean[][] nextWorld;
	private int width;
	private int height;
	private int countGeneration = 0;
	private static int[][] murderer = {
		{1,1,0,1,1,0,1,0,0,1,0,1,1,1,1},
		{1,0,1,0,1,0,1,0,0,1,0,1,0,0,1},
		{1,0,0,0,1,0,1,0,0,1,0,1,1,1,1},
		{1,0,0,0,1,0,1,0,0,1,0,1,0,1,0},
		{1,0,0,0,1,0,1,1,1,1,0,1,0,0,1},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,1,1,0,0,0,1,1,1,1,0,1,1,1,1},
		{1,0,0,1,0,0,1,0,0,0,0,1,0,0,1},
		{1,0,0,0,1,0,1,1,1,1,0,1,1,1,1},
		{1,0,0,1,0,0,1,0,0,0,0,1,0,1,0},
		{1,1,1,0,0,0,1,1,1,1,0,1,0,0,1},
		{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		{1,1,1,1,0,0,1,1,1,1,0,0,1,0,0},
		{1,0,0,0,0,0,1,0,0,1,0,0,1,0,0},
		{1,1,1,1,0,0,1,1,1,1,0,0,1,0,0},
		{1,0,0,0,0,0,1,0,1,0,0,0,0,0,0},
		{1,1,1,1,0,0,1,0,0,1,0,0,1,0,0}
	};
	private static int gsize = 700;

	public static void main(String[] args) {
		GameOfLive game = new GameOfLive();
		game.setSize(gsize);
		setMurderer(game);
		//System.out.println(game);

		boolean[][] w = game.getWorld();
		while (true) {
			game.nextGeneration();
			boolean[][] w1 = game.getWorld();
			if (isEqual(w, w1)) {
				System.out.println(game.getCountGeneration());
				System.out.println(getLivingCells(w));
				System.out.println();
			}
			w = w1;
		}
	}


	private static void setMurderer(final GameOfLive game) {
		int startx = gsize/2;
		int starty = gsize/2;

		for (int y = 0; y < murderer.length; y++) {
			for (int x = 0; x < murderer[y].length; x++) {
				if (murderer[y][x] == 1) {
					game.set(x+startx, y+starty, true);
				}
			}
		}
	}

	public void set(final int x, final int y, final boolean b) {
		set(x, y, b, world);
	}

	private static boolean isEqual(final boolean[][] w, final boolean[][] w1) {
		return getLivingCells(w) == getLivingCells(w1);
	}

	private static int getLivingCells(final boolean[][] w1) {
		int count = 0;
		for (int y = 0; y < w1.length; y++) {
			for (int x = 0; x < w1[y].length; x++) {
				if (w1[y][x]) {
					count++;
				}
			}
		}
		return count;
	}

	private void setVerticalLine(final int x, final int y, final int length) {
		for (int i = y; i < y+length; i++) {
			set(x, i, true, world);
		}
	}

	private void setHorizontalLine(final int x, final int y, final int length) {
		for (int i = x; i < x+length; i++) {
			set(i, y, true, world);
		}
	}

	private void setSize(final int size) {
		world = new boolean[size][size];
		nextWorld = new boolean[size][size];
		width = size;
		height = size;
	}

	public void nextGeneration() {
		for (int y = 0; y < width; y++) {
			for (int x = 0; x < width; x++) {
				set(x, y, isAlive(x, y), nextWorld);
			}
		}
		copyNextToCurrentWorld();
		countGeneration++;
	}

	private boolean isAlive(final int x, final int y) {
		int numberOfLiveNeighbours = getNeighbours(x, y);
		return (numberOfLiveNeighbours == 2 && get(x, y, world))|| numberOfLiveNeighbours == 3 ;
	}

	private int getNeighbours(final int x, final int y) {
		int count = 0;
		if (isNeighbourAlive(x-1, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x-1, y)) {
			count++;
		}
		if(isNeighbourAlive(x-1, y+1)) {
			count++;
		}
		if(isNeighbourAlive(x, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x, y+1)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y-1)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y)) {
			count++;
		}
		if(isNeighbourAlive(x+1, y+1)) {
			count++;
		}
		return count;
	}

	private boolean isNeighbourAlive(final int x, final int y) {
		if (get(x, y, world)) {
			return true;
		} else {
			return false;
		}
	}

	private void copyNextToCurrentWorld() {
		for (int y = 0; y < width; y++) {
			for (int x = 0; x < width; x++) {
				set(x, y, get(x, y, nextWorld) , world) ;
			}
		}
	}

	private void set(int x, int y, boolean b, boolean[][] world) {
		world[y][x] = b;
	}

	private boolean get(int x, int y, boolean[][] world) {
		if (x < 0 || x >= width || y < 0  || y >= height) {
			return false;
		} else {
			return world[y][x];
		}
	}

	public boolean[][] getWorld() {
		boolean result[][] = new boolean[world.length][world[0].length];
		for (int y = 0; y < world.length; y++) {
			result[y] = world[y].clone();
		}
		return result;
	}

	public String toString() {
		return toString(world);
	}
	public String toString(boolean[][] map) {
		StringBuffer result = new StringBuffer();
		result.append(' ');

		for (int i = 0; i < width; i++) {
			result.append(i % 10);
		}
		result.append('\n');

		for (int y = 0; y < height; y++) {
			result.append(y % 10);
			for (int x = 0; x < width; x++) {
				result.append(get(x, y, map) ? '*' : ' ');
			}
			result.append(y%height);
			result.append("\n");
		}

		result.append(' ');
		for (int i = 0; i < width; i++) {
			result.append(i%10);
		}
		result.append('\n');
		return result.toString();
	}

	public int getCountGeneration() {
		return countGeneration;
	}
}