package jm.math;

import java.util.ArrayList;
import java.util.List;

/**
 * User: T05365A
 * Date: 04.03.11
 * Time: 09:05
 *
 * Class for matrices: addition, scalar multiplication and transpose
 */
public class Matrix {
	private List<List<Integer>> matrix = new ArrayList<List<Integer>>();

	public Matrix add(Matrix m) {
		Matrix result = new Matrix();
		if (m.getColumnCount() == getColumnCount() && m.getRowCount() == getRowCount()) {
			for (int i = 0; i < getRowCount(); i++) {
				List<Integer> row = getRow(i);
				List<Integer> row1 = m.getRow(i);
				List<Integer> sum = new ArrayList<Integer>();

				for (int j = 0; j < row.size(); j++) {
					sum.add(row.get(j) + row1.get(j));
				}

				result.addRow(sum);
			}
		}
		return result;
	}

	public Matrix multiply(Integer m) {
		Matrix result = new Matrix();
		for (List<Integer> row : matrix) {
			List<Integer> mRow = new ArrayList<Integer>();
			for (Integer i : row) {
				mRow.add(i*m);
			}
			result.addRow(mRow);
		}
		return result;
	}

	public Matrix transpose() {
		Matrix result = new Matrix();

		for (int row = 0; row < getRowCount(); row++) {
			result.addColumn(getRow(row));
		}

		return result;
	}

	public Matrix rotate() {
		Matrix result = new Matrix();

		for (int i = getRowCount() - 1; i >= 0; i--) {
			List<Integer> row = getRow(i);
			List<Integer> col = new ArrayList<Integer>();

			for (int j = 0; j < row.size(); j++) {
				col.add(row.get(j));
			}

			result.addColumn(col);
		}

		return result;
	}
	public void addRow(List<Integer> row) {
		if (isValidRow(row) || isEmpty()) {
			matrix.add(row);
		}
	}

	public void addColumn(List<Integer> col) {
		if (isEmpty()) {
			for (int i = 0; i < col.size(); i++) {
				List<Integer> row = new ArrayList<Integer>();
				row.add(col.get(i));
				addRow(row);
			}
		} else if (isValidColumn(col)) {
			for (int i = 0; i < col.size(); i++) {
				getRow(i).add(col.get(i));
			}
		}
	}

	private boolean isEmpty() {
		return matrix.isEmpty();
	}

	private boolean isValidColumn(final List<Integer> col) {
		return col.size() == matrix.size();
	}

	private boolean isValidRow(final List<Integer> row) {
		return row.size() == getColumnCount();
	}

	public List<Integer> getRow(int row) {
		if (isValidRow(row)) {
			return matrix.get(row);
		} else {
			return new ArrayList<Integer>();
		}
	}

	public List<Integer> getColumn(int col) {
		if (isValidColumn(col)) {
			List<Integer> column = new ArrayList<Integer>();

			for (List<Integer> row : matrix) {
				column.add(row.get(col));
			}
			return column;
		} else {
			return new ArrayList<Integer>();
		}
	}

	public void setRow(int row, List<Integer> value) {
		if (isValidRow(row)) {
			matrix.set(row, value);
		}
	}

	private boolean isValidColumn(final int col) {
		if (isValidRow(0)) {
			return getRow(0).size() > col && col >= 0;
		} else {
			return false;
		}
	}

	private boolean isValidRow(final int row) {
		return matrix.size() > row && row >= 0;
	}

	public int getColumnCount() {
		if (isValidRow(0)) {
			return getRow(0).size();
		} else {
			return 0;
		}
	}

	public int getRowCount() {
		return matrix.size();
	}

	public String toString() {
		StringBuffer result = new StringBuffer();
		for (List<Integer> row : matrix) {
			result.append(row.toString() + "\n");
		}
		return result.toString();
	}

	public boolean equals(Matrix m) {
		if (getRowCount() != m.getRowCount() || getColumnCount() != m.getColumnCount()) {
			return false;
		}

		for (int r = 0; r < getRowCount(); r++) {
			List<Integer> row = getRow(r);
			List<Integer> row1 = m.getRow(r);
			for (int c = 0; c < row.size(); c++) {
				if (row.get(c) != row1.get(c)) {
					return false;
				}
			}
		}
		return true;
	}

    public static long getDeterminante(long[][] matrix, int row) {
        if (matrix.length == 1 && matrix[0].length == 1) {
            return matrix[0][0];
        }

        int det = 0;
        int s = row %2 == 0 ? 1 : -1;
        for (int col = 0; col < matrix[0].length; col++) {
            long value = matrix[row][col];
            det += s * value * getDeterminante(getDetMatrix(matrix, row, col), row);
            s *= -1;
        }

        return det;
    }

    public static long getDeterminante(long[][] matrix) {
        return getDeterminante(matrix, 0);

    }

    private static long[][] getDetMatrix(long[][] matrix, int row, int col) {
        int matrixLength= matrix.length;
        long[][] detMatrix = new long[matrixLength - 1][matrixLength - 1];

        int isub = 0;
        int ksub;
        for (int i = 0; i < matrixLength; i++) {
            if (i == row) {
                isub = 1;
                continue;
            }
            ksub = 0;
            for (int k = 0; k < matrixLength; k++) {
                if (k == col) {
                    ksub = 1;
                    continue;
                }

                detMatrix[i-isub][k-ksub] = matrix[i][k];
            }
        }

        return detMatrix;
    }

    public static long[][] getTranspositionMatrix(long[][] matrix) {
        long[][] transpositionMatrix = new long[matrix.length][matrix[0].length];

        for (int row = 0; row < transpositionMatrix.length; row++) {
            for (int col = row; col < transpositionMatrix[row].length; col++) {
                transpositionMatrix[col][row] = matrix[row][col];
                transpositionMatrix[row][col] = matrix[col][row];
            }
        }

        return transpositionMatrix;
    }

    public static long[][] getInverseMatrix(long[][] matrix) {
        long[][] inverseMatrix = new long[matrix.length][matrix[0].length];

        for (int row = 0; row < matrix.length; row++) {
            for (int col = 0; col < matrix[row].length; col++) {

                int sign = (row+col) % 2 == 0 ? 1 : -1;
                inverseMatrix[row][col] = sign * getDeterminante(getDetMatrix(matrix, row, col));
            }
        }

        return getTranspositionMatrix(inverseMatrix);
    }

    public static void main(String[] args) {
        long[][] matrix = {
                {6,24,1},
                {13,16,10},
                {20,17,15}

        };

        //System.out.println(Matrix.getDeterminante(matrix));
        //System.out.println(Matrix.getDeterminante(matrix, 1));

        //System.out.println(Matrix.toString(Matrix.getTranspositionMatrix(matrix)));

        ModuloArithmetik modul = new ModuloArithmetik(26);
        long det = getDeterminante(matrix);
        long detM = modul.getModulo(det);
        System.out.println("Det = " + det + " -> " + detM );
        long[][] inverse = getInverseMatrix(matrix);
        for (long[] ints : inverse) {
            for (long anInt : ints) {
                System.out.print(modul.getModulo(anInt*detM) + " ");
            }
            System.out.println();
        }
        System.out.println();

        modul.getInverseMatrix(matrix);

        int[] numbers = {112, 123, 51, 127};
        System.out.print(">");
        for (int number : numbers) {
            System.out.print((char)number);
        }
        System.out.print("<");
	}

    public static String toString(long[][] matrix) {
        StringBuilder builder = new StringBuilder();
        for (long[] ints : matrix) {
            for (long anInt : ints) {
                builder.append(anInt + " ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

	public static String toString(char[][] matrix) {
		StringBuilder builder = new StringBuilder();
		for (char[] chars : matrix) {
			for (char aChar : chars) {
				builder.append(aChar + " ");
			}
			builder.append("\n");
		}
		return builder.toString();
	}
}
