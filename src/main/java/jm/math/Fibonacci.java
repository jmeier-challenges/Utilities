package jm.math;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Methods for fibonacci numbers
 * User: johann
 * Date: 31.12.10
 * Time: 09:58
 */
public class Fibonacci {
    private List<BigInteger> fibos = new ArrayList<BigInteger>();

    public Fibonacci() {
        fibos.add(BigInteger.ZERO);
        fibos.add(BigInteger.ONE);
    }

    public static BigInteger fiboBino(int n1) {
        int n = n1-1;
        int max;
        if (n % 2 == 0) {
            // even
            max = n/2;
        } else {
            max = (n-1)/2;
        }
        BigInteger sum = BigInteger.ZERO;
        for (int k = 0; k <= max; k++) {
            sum = sum.add(Combinations.nOverK(n-k,k));
        }
        return sum;
    }

    public BigInteger fibonacci(int n) {
        BigInteger fib =  null;
        for (int i = 0; i <= n; i++) {
            fib =  fib(i);
            if (i >= fibos.size()) {
                fibos.add(fib);
            }
        }
        return fib;
    }

    private BigInteger fib(int n) {
        if (fibos.size() > n) {
            return fibos.get(n);
        }
        return fib(n-1).add(fib(n-2));
    }

    public BigInteger fibMatrix(long count) {
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ZERO;
        BigInteger p = BigInteger.ZERO;
        BigInteger q = BigInteger.ONE;

        while (count > 0) {
            if (count % 2 == 0) {
                p = p.multiply(p).add(q.multiply(q));
                q = p.multiply(q).multiply(BigInteger.valueOf(2)).add(q.multiply(q));
                count = count/2;
            } else {
                a = b.multiply(q).add(a.multiply(q)).add(a.multiply(p));
                b = b.multiply(p).add(a.multiply(q));
                count = count - 1;
            }
        }

        return b;
    }
    int Fib(int count) {
        int a = 1;
        int b = 0;
        int p = 0;
        int q = 1;

        while (count > 0) {
            if (count % 2 == 0) {
                p = p * p + q * q;
                q = 2 * p * q + q * q;
                count = count / 2;
            } else {
                a = b * q + a * q + a * p;
                b = b * p + a * q;
                count = count - 1;
            }
        }

        return b;
    }
    /*
    fib(2*n) = fib(n) * (fib(n) + 2 * fib(n-1))
    fib(2*n-1) = fib(n)^2 + fib(n-1)^2

    fib(2*n) = fib(n)*(fib(n+1)+fib(n-1))
    fib(2*n+1) = fib(n)^2 + fib(n+1)^2


     */
    /*
Function Fib(count)
    a ? 1
    b ? 0
    p ? 0
    q ? 1

    While count > 0 Do
        If Even(count) Then
             p ? p² + q²
             q ? 2pq + q²
             count ? count ÷ 2
        Else
             a ? bq + aq + ap
             b ? bp + aq
             count ? count - 1
        End If
    End While

    Return b
End Function
     */

    public static void main(String[] args) {
        Fibonacci fibo = new Fibonacci();
        System.out.println(fibo.fibonacci(0));
        System.out.println(fibo.fibonacci(1));
        System.out.println(fibo.fibonacci(2));
        System.out.println(fibo.fibonacci(3));
        System.out.println(fibo.fibonacci(4));
        System.out.println(fibo.fibonacci(5));
        System.out.println(fibo.Fib(2));
        System.out.println(fibo.fibMatrix(2));
    }

    public List<BigInteger> getFirst(int i) {
        fibonacci(i-1);
        return fibos;
    }

	public List<BigInteger> getLessThan(BigInteger max) {
		List<BigInteger> fibos = new ArrayList<BigInteger>();

		BigInteger f_0 = BigInteger.ZERO;
		BigInteger f_1 = BigInteger.ONE;

		fibos.add(f_0);
		fibos.add(f_1);

		BigInteger f = f_1;
		do {
			f = f_0.add(f_1);
			if (max.compareTo(f) > 0) {
				fibos.add(f);
			}

			f_1 = f_0;
			f_0 = f;
		} while (max.compareTo(f) > 0);

		return fibos;
	}
}
