package jm.math;

import java.math.BigInteger;
import java.security.InvalidParameterException;

/**
 * User: johann
 * Date: 01.01.11
 * Time: 15:16
 */
public class Combinations {
    public static BigInteger fac(final BigInteger n) {
        if (BigInteger.ZERO.equals(n)) {
            return BigInteger.ONE;
        }
        BigInteger fac = BigInteger.ONE;
        BigInteger i = BigInteger.ONE;
        while (i.compareTo(n) <= 0) {
            fac = fac.multiply(i);
            i = i.add(BigInteger.ONE);
        }
        return fac;
    }

    public static BigInteger nOverK(int n, int k) {
        if (n < 0 || k < 0 || n < k) {
            throw new InvalidParameterException("Invalid values for n over k! n = " + n + ", k = " + k);
        }
        BigInteger sum = BigInteger.ONE;
        BigInteger i = BigInteger.valueOf(n-k+1);
        BigInteger m = BigInteger.valueOf(n);
        while (i.compareTo(m) <= 0) {
            sum = sum.multiply(i);
            i = i.add(BigInteger.ONE);
        }
        sum = sum.divide(fac(BigInteger.valueOf(k)));

        return sum;
    }

    public static void main(String[] args) {
        System.out.println(fac(BigInteger.valueOf(199)));
    }
}
