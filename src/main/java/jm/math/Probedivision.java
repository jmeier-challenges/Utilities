package jm.math;

import java.math.BigInteger;
import java.util.*;

/**
 * User: T05365A
 * Date: 15.04.11
 * Time: 15:01
 *
 * Faktorisierung
 */
public class Probedivision {

	public static void main(String[] args) {


		//long n = 999999999999999991L;
        long n = 243;
		Probedivision probe = new Probedivision();
		List<Long> faktors = probe.getFactorization(n);
		System.out.println(probe.toString(faktors));
	}

	public String toString(final List<Long> divisors) {
		StringBuffer result = new StringBuffer();
		long divisor = divisors.get(0);
		long lastDivisor = divisor;
		int count = 1;
		for (int i = 1; i < divisors.size(); i++) {
			divisor = divisors.get(i);
			if (lastDivisor == divisor) {
				count++;
			} else {
				if (count == 1) {
					result.append(lastDivisor).append(" x ");
				} else {
					result.append(lastDivisor).append(" ^ ").append(count).append(" x ");
				}
				count = 1;
				lastDivisor = divisor;
			}
		}
		if (count == 1) {
			result.append(lastDivisor);
		} else {
			result.append(lastDivisor).append(" ^ ").append(count);
		}
		return result.toString();
	}

	public List<Long> getFactorization(long number) {
		List<Long> factors = new ArrayList<Long>();
		long n = number;
		long i = 2;

		long sqrN = (long) Math.sqrt(n)+1;
		while (i < sqrN) {
			if (n % i == 0) {
				factors.add(i);
				n = n/i;
				sqrN = (long) Math.sqrt(n)+1;
			} else {
				i++;
			}
		}
		factors.add(n);

		return factors;
	}

	public List<Long> getDivisors1(long number) {
		List<Long> divisors = new ArrayList<Long>();
		long end = (long) Math.sqrt(number);
		for (long i = 1; i <= end; i++) {
			if (number % i == 0) {
				divisors.add(i);
			}
		}
		int e = divisors.size();
		for (int i = e-1; i >= 0; i--) {
			divisors.add(number/divisors.get(i));
		}
		return divisors;
	}

    public List<Long> getDivisors2(long number) {
        List<Long> divisors = new ArrayList<Long>();
        List<Long> factors = getFactorization(number);

        divisors.add(1L);
        divisors.add(number);
        for (int i = 1; i < factors.size(); i++) {
            getDivisors2Rec(0, i, 1, factors, divisors);
        }
        Collections.sort(divisors);
        return divisors;
    }

    public List<BigInteger> getDivisors2(List<BigInteger> factors) {
        List<BigInteger> divisors = new ArrayList<BigInteger>();
        BigInteger number = factors.get(0);

        divisors.add(BigInteger.ONE);
        for (int i = 1; i < factors.size(); i++) {
            number = number.multiply(factors.get(i));
            getDivisors2LongRec(0, i, BigInteger.ONE, prepare(factors, i), divisors);
        }
        Collections.sort(divisors);
        divisors.add(number);
        return divisors;
    }

    /**
     * Jeden Teiler höchsten n-mal in der Liste
     * @param divisors
     * @param n
     * @return
     */
    private List<BigInteger> prepare(List<BigInteger> divisors, int n) {
        int count = 1;
        BigInteger last = BigInteger.ZERO;
        List<BigInteger> preparedList = new ArrayList<BigInteger>();
        for (BigInteger divisor : divisors) {
            if (last.equals(divisor)) {
                count++;
            } else {
                count = 1;
                last = divisor;
            }
            if (count <= n) {
                preparedList.add(divisor);
            }
        }
        return preparedList;
    }

    private void getDivisors2LongRec(int start, int length, BigInteger factor,  List<BigInteger> factors, List<BigInteger> divisors) {
        if (length == 0) {
            return;
        }

        for (int i = start; i < factors.size(); i++) {
            BigInteger f = factors.get(i).multiply(factor);
            if (!divisors.contains(f)) {
                divisors.add(f);
            }
            getDivisors2LongRec(i + 1, length - 1, f, factors, divisors);
        }
    }

    private void getDivisors2Rec(int start, int length, long factor,  List<Long> factors, List<Long> divisors) {
        if (length == 0) {
            return;
        }

        for (int i = start; i < factors.size(); i++) {
            long f = factors.get(i)*factor;
            if (!divisors.contains(f)) {
                divisors.add(f);
            }
            getDivisors2Rec(i + 1, length - 1, f, factors, divisors);
        }
    }

    /**
     * Teilersumme (Sum of Divisors)
     * @param primefactors List of primefactors
     * @return
     */
    public BigInteger sigma(List<BigInteger> primefactors) {
        BigInteger sum = BigInteger.ONE;
        BigInteger last = primefactors.get(0);
        int count = 1;

        BigInteger bi;
        BigInteger primefactor = BigInteger.ONE;
        for (int i = 1; i < primefactors.size(); i++) {
            primefactor = primefactors.get(i);
            if (primefactor.equals(last)) {
                count++;
            } else {
                bi = last.pow(count + 1).subtract(BigInteger.ONE);
                sum = sum.multiply(bi.divide(last.subtract(BigInteger.valueOf(1))));
                count = 1;
                last = primefactor;
            }
        }
        bi = primefactor.pow(count + 1).subtract(BigInteger.ONE);
        sum = sum.multiply(bi.divide(last.subtract(BigInteger.valueOf(1))));
        return sum;
    }

}