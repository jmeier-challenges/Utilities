package jm.math;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * User: johann
 * Date: 16.03.11
 * Time: 06:53
 */
public class Summand {
    
    
    List<List<Integer>> summs = new ArrayList<List<Integer>>();


    public static void main(String[] args) {
        Summand s = new Summand();
        List<Integer> summands = new ArrayList<Integer>();
        summands.add(6);
        summands.add(3);
        summands.add(2);
        List<List<Integer>> sol = s.getSummands(16, summands);
        for (List<Integer> integers : sol) {
            System.out.println(integers);
        }
        
    }
    
    public List<List<Integer>> getSummands(int sum, final List<Integer> summands) {
         
        Collections.sort(summands);
        Collections.reverse(summands);

        List<Integer> sums = new ArrayList<Integer>();
        process1(sum, sums, summands);
        
        return summs;
    }

    private void process(int sum, List<Integer> sums, List<Integer> summands) {
        if (sum == 0) {
            summs.add(sums);         
            return;
        }

        for (Integer summand : summands) {
            if (sum - summand >= 0) {
                List<Integer> newSums = new ArrayList<Integer>(sums);
                newSums.add(summand);
                process(sum-summand, newSums, summands);
            }
        }
    }

    private void process1(int sum, List<Integer> sums, List<Integer> summands) {
        if (sum == 0) {
            summs.add(sums);
            return;
        }

        if (summands.size() == 0) {
            return;
        }

            Integer max = summands.get(0);
            if (sum - max >= 0) {
                List<Integer> newSums = new ArrayList<Integer>(sums);
                newSums.add(max);
                process1(sum-max, newSums, summands);
            }

            if (summands.size() > 1) {
                process1(sum, sums, summands.subList(1, summands.size()));
            }
    }
}
