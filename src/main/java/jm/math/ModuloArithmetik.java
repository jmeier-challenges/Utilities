package jm.math;

import java.math.BigInteger;

/**
 * User: T05365A
 * Date: 06.07.12
 * Time: 08:28
 */
public class ModuloArithmetik {
    private long modul;

    public ModuloArithmetik(long modul) {
        setModul(modul);
    }

    //  Der Algorithmus geht nur, falls a und modul teilerfremd sind.
    public Long getInverse(Long a) {
        long[] egcd = extendedGcd(modul, a);
        if (egcd[0] > 1) {
            return -1L;
        } else {
            return getModulo(egcd[2]);
        }
    }

    public long getModulo(long i) {
        while (i < 0) {
            i += modul;
        }
        return i % modul;
    }

    public static long[] extendedGcd(long a, long b) {
        long[] auv = new long[3];
        long x = 0, lastx = 1;
        long y = 1, lasty = 0;

        while (b > 0) {
            long quotient = a / b;
            long rest = a % b;
            a = b;
            b = rest;

            long x1 = lastx - quotient * x;
            lastx = x;
            x = x1;

            long y1 = lasty - quotient * y;
            lasty = y;
            y = y1;
        }

        auv[0] = a;
        auv[1] = lastx;
        auv[2] = lasty;

        return auv;
    }

    public static long ggt(long a, long b) {
        while (b > 0) {
            long rest = a % b;
            a = b;
            b = rest;
        }
        return a;
    }


    public long[][] getInverseMatrix(long[][] matrix){
        long detM = getModulo(Matrix.getDeterminante(matrix));
        if (ggt(detM, modul) > 1) {
            throw new RuntimeException("Matrix has no inverse!");
        }
        long[][] inverse = Matrix.getInverseMatrix(matrix);

        for (int j = 0; j < matrix.length; j++) {
            long[] ints = inverse[j];
            for (int k = 0; k < ints.length; k++) {
                long anInt = ints[k];
                inverse[j][k] = getModulo(anInt * getInverse(detM));
            }
        }
        return inverse;
    }

    public long[] multiply(long[][] matrix, long[] vector) {
        long[] result = new long[vector.length];

        for (int j = 0; j < matrix.length; j++) {
            int value = 0;
            for (int k = 0; k < matrix[j].length; k++) {
                value += matrix[j][k] * vector[k];
            }
            result[j] = getModulo(value);
        }

        return result;
    }

    public long[][] multiply(long[][] m1, long[][] m2) {
        long[][] result = new long[m2.length][m2[0].length];

        long[] vec;
        for (int col = 0; col < m2[0].length; col++) {
            vec = getVector(m2, col);
            long[] vec1 = multiply(m1, vec);
            for (int j = 0; j < vec1.length; j++) {
                result[j][col] = vec1[j];
            }
        }

        return result;
    }

    private static long[] getVector(long[][] m2, int col) {
        long[] vec = new long[m2.length];
        for (int row = 0; row < m2.length; row++) {
            vec[row] = m2[row][col];
        }
        return vec;
    }

    public static void main(String[] args) {

	    int x = 27;
	    int n = 29;
	    int mod = 100;

	    ModuloArithmetik moduloArithmetik = new ModuloArithmetik(mod);
	    System.out.println(moduloArithmetik.pow(x, n));

	    BigInteger xBig = BigInteger.valueOf(x);
	    BigInteger resultBig = xBig.pow(n);
	    System.out.println(resultBig.mod(BigInteger.valueOf(mod)));


    }

    private static void solve() {
        ModuloArithmetik arithmetik = new ModuloArithmetik(26);
        long[][] ptMatrix = {
                {4, 3},
                {13, 8}
        };

        long[][] ctMatrix = {
                {15, 5},
                {19, 8}
        };


        long[][] inversPtMatrix = arithmetik.getInverseMatrix(ptMatrix);

        long[][] keyMatrix = arithmetik.multiply(ctMatrix, inversPtMatrix);

        System.out.println(Matrix.toString(keyMatrix));
    }

    public long getModul() {
        return modul;
    }

    public void setModul(long modul) {
        this.modul = modul;
    }

	public long pow(long x, long n) {
		long result = 1;
		for (int i = 0; i < n; i++) {
			result *= x;
			result = getModulo(result);
		}
		return result;
	}

}
