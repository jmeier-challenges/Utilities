package jm.math;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Function for Triangle Numbers : n(n+1)/2
 * User: johann
 * Date: 01.01.11
 * Time: 17:27
 */
public class TriangleNumber {
    public static BigInteger getTriangleNumber(int n) {
        BigInteger bigN = BigInteger.valueOf(n);
        return bigN.multiply(bigN.add(BigInteger.ONE)).divide(BigInteger.valueOf(2));
    }

    public static List<BigInteger> getFirst(int n) {
        List<BigInteger> numbers = new ArrayList<BigInteger>();
        for (int i = 1; i <= n; i++) {
            numbers.add(getTriangleNumber(i));
        }
        return numbers;
    }

}
